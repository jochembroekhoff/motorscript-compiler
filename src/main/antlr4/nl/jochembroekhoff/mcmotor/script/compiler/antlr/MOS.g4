/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * MotorScript grammar for ANTLR4
 */

grammar MOS ;

/*
 * Parser rules
 */

script : ( compilerInstruction )* ( statement | functionDeclaration )+ EOF ;

/* Compiler instruction & Annotation */

compilerInstruction
    : Hash LParen Identifier Assign expression RParen
    ;

annotation
    : Hash properties
    ;

/* Blocks & statements */

block
    : LAcc ( statement )* RAcc
    | statement
    ;

statement
    : declarationStatement
    | assignStatement
    | forStatement
    | ifStatement
    | executeStatement
    | callStatement
    | rawStatement
    | scheduleStatement
    | reference
    ;

/* Declaration statement */

declarationStatement
    : annotation? ( Const | Var ) Identifier Assign expression
    ;

/* Assign statement */

assignStatement
    : reference assignment expression
    ;

assignment
    : Assign
    | AssignMultiply
    | AssignDivide
    | AssignModulus
    | AssignPlus
    | AssignMinus
    ;

/* For statement */

forStatement
    : forInfinite
    | forIn
    ;

forInfinite
    : For block
    ;

forIn
    : For Identifier ( Comma Identifier )? InSgn forExpression block
    ;

forExpression
    : reference
    | range
    | nbt
    ;

/* If statement */

ifStatement
    : ifStatementFirstBlock ifStatementElseIfBlock* ifStatementElseBlock?
    ;

ifStatementFirstBlock
    : If expression block
    ;

ifStatementElseIfBlock
    : Else If expression block
    ;

ifStatementElseBlock
    : Else block
    ;

/* Execute statement */

executeStatement
    : executeChainPart+ block
    ;

executeChainPart
    : executeAs
    | executeAt
//  | executeFacing
    | executeIn
    | executePositioned
    ;

executeAs
    : As selector
    ;

executeAt
    : At selector
    ;

//executeFacing
//  : 'facing' position
//  | 'facing' 'entity' selector ( 'eyes' | 'feet' )
//  ;

executeIn
    : In resource
    ;

executePositioned
    : Positioned position
    | Positioned As selector
    ;

/* Call statement */

callStatement
    : Call expression
    ;

/* Schedule statement */

scheduleStatement
    : Schedule expression block
    ;

/* Raw statement */

rawStatement
    : Raw expression ( Comma expression )*
    ;

/* Expression */

expression
    : LParen expression RParen
    | expression expressionMulDivMod expression
    | expression expressionPlusMinus expression
    | expression equalityOperator expression
    | expression ( conditionalAnd expression )+
    | expression ( conditionalOr expression )+
    | Exclam expression
    | reference
    | selector
    | literalValue
    | range
    | nbt
    | path
    | position
    | resource
    ;

expressionMulDivMod
    : Multiply
    | FSlash
    | Modulus
    ;

expressionPlusMinus
    : Plus
    | Minus
    ;

/* Conditions and Equality */

conditionalOr
    : OpOr
    ;

conditionalAnd
    : OpAnd
    ;

equalityOperator
    : OpLessThanOrEqualTo
    | OpLessThan
    | OpEqualTo
    | OpNotEqualTo
    | OpGreaterThanOrEqualTo
    | OpGreaterThan
    | OpMatches
    ;

/* References */

reference
    : ( referenceVarOrConst | referenceInternal ) referencePart*
    ;

referenceVarOrConst
    : Identifier
    ;

referenceInternal
    : BSlash ( Identifier | Schedule )
    ;

referencePart
    : path referenceInvocation
    | path
    | referenceInvocation
    ;

referenceInvocation
    : LParen arguments? RParen
    ;

arguments
    : ( argumentUnnamed ( Comma argumentUnnamed )* )+ ( Comma argumentNamed )*
    | argumentNamed ( Comma argumentNamed )*
    ;

argumentUnnamed
    : expression
    ;

argumentNamed
    : Identifier Assign expression
    ;

/* Selector */

selector
    : ( selectorDefault | selectorEntityName ) properties?
    ;

selectorDefault : SelectorDefault ;
selectorEntityName : AtSgn Identifier ;

scoresMap
    : LAcc scoresMapKeyValue? ( Comma scoresMapKeyValue )* RAcc
    ;

scoresMapKeyValue
    : Identifier '=' range
    ;

/* Range */

range
    : rangeExact
    | rangeFromTo
    | rangeOrBelow
    | rangeOrAbove
    ;

rangeExact : rangePart ;
rangeFromTo : rangePart DotDot rangePart ;
rangeOrBelow : DotDot rangePart ;
rangeOrAbove : rangePart DotDot ;

rangePart
    : literalNumeric
    | reference
    ;

/* NBT */

nbt
    : nbtCompound
    | nbtArray
    | nbtList
    | reference
    | literalValue
    ;

nbtCompound
    : LAcc nbtCompoundKeyValue* RAcc
    ;

nbtCompoundKeyValue
    : nbtCompoundKey Colon nbtCompoundValue Comma?
    ;
nbtCompoundKey
    : Identifier
    | String
    ;
nbtCompoundValue
    : nbt
    ;

nbtArray
    : LBrack nbtArrayType SColon ( nbtArrayValue ( Comma nbtArrayValue )* )? RBrack
    ;
nbtArrayType
    : 'B' // Byte array
    | 'I' // Int array
    | 'L' // Long array
    ;
nbtArrayValue
    : literalByte
    | literalInteger
    | literalLong
    ;

nbtList
    : LBrack ( nbtListValue ( Comma nbtListValue )* )? RBrack
    ;
nbtListValue
    : nbt
    ;

/* Path */

path
    : pathSegment+
    ;

pathSegment
    : Dot pathSegmentString
    | Dot? pathSegmentIndex
    ;

pathSegmentString
    : Identifier
    | String
    ;

pathSegmentIndex
    : LBrack ( Integer | expression ) RBrack
    ;

/* Position */

position
    : Btick positionPart positionPart positionPart Btick
    ;

positionPart
    : expression
    | positionOffsetTilde
    | positionOffsetCaret
    ;

positionOffsetTilde
    : Tilde expression?
    ;

positionOffsetCaret
    : Caret expression?
    ;

/* Resource */

resource
    : Dollar ( ( Identifier ( Dot Identifier )* )? Colon )? resourceName properties? nbtCompound?
    ;

resourceName
    : Identifier ( FSlash Identifier )*
    ;

/* Literal value */

literalValue
    : literalString
    | literalBoolean
    | literalNumeric
    ;

literalNumeric
    : literalDecimal
    | literalWhole
    ;

literalDecimal
    : literalDouble
    | literalFloat
    ;

literalWhole
    : literalLong
    | literalInteger
    | literalShort
    | literalByte
    ;

literalString : String ;
literalBoolean : Boolean ;
literalDouble : Double ;
literalFloat : Float ;
literalLong : Long ;
literalInteger : Integer ;
literalShort : Short ;
literalByte : Byte ;

/* Properties */

properties
    : LBrack ( propertyKeyValue ( Comma propertyKeyValue )* )? RBrack
    ;

propertyKeyValue
    : Identifier ( OpNotEqualTo | Assign ) ( scoresMap | expression )
    ;

/* Signature */

signature
    : signaturePart ( Comma signaturePart )*
    ;

signaturePart
    : signatureParameterName signatureParameterType?
    ;

signatureParameterName
    : Identifier
    ;

signatureParameterType
    : Identifier
    ;

/* Functions */

functionDeclaration
    : functionReturnType functionName LParen signature? RParen block
    ;

functionReturnType
    : Identifier
    ;

functionName
    : Identifier
    ;

/*
 * Lexer rules
 */

// something something
Dollar : '$' ;
AtSgn  : '@' ;
LBrack : '[' ;
RBrack : ']' ;
LAcc   : '{' ;
RAcc   : '}' ;
LParen : '(' ;
RParen : ')' ;
Colon  : ':' ;
SColon : ';' ;
DotDot : '..';
Dot    : '.' ;
Comma  : ',' ;
Btick  : '`' ;
Tilde  : '~' ;
Caret  : '^' ;
Hash   : '#' ;
Exclam : '!' ;
FSlash : '/' ;
BSlash : '\\' ;

// KEYWORDS
Var        : 'var' ;
Const      : 'const' ;
For        : 'for' ;
InSgn      : '<-' ;
If         : 'if' ;
Else       : 'else' ;
As         : 'as' ;
At         : 'at' ;
In         : 'in' ;
Call       : 'call' ;
Raw        : 'raw' ;
//Facing     : 'facing' ;
Positioned : 'positioned' ;
Schedule   : 'schedule' ;

// ASSIGNMENT
Assign         : '=' ;
AssignPlus     : '+=' ;
AssignMinus    : '-=' ;
AssignMultiply : '*=' ;
AssignDivide   : '/=' ;
AssignModulus  : '%=' ;

// OPERATIONS
Plus     : '+' ;
Minus    : '-' ;
Multiply : '*' ;
// Divide   : FSlash ;
Modulus  : '%' ;

// OPERATORS
OpOr                   : '||'      ;
OpAnd                  : '&&'      ;
OpLessThanOrEqualTo    : '<='      ;
OpLessThan             : '<'       ;
OpEqualTo              : '=='      ;
OpNotEqualTo           : '!='      ;
OpGreaterThanOrEqualTo : '>='      ;
OpGreaterThan          : '>'       ;
OpMatches              : 'matches' ;

// SELECTOR
SelectorDefault
    : AtSgn ( 'e' | 'r' | 'p' | 's' | 'a' )
    ;

// STRING
// Inspiration: https://github.com/antlr/grammars-v4/blob/master/java8/Java8.g4
String
    : '"' StringCharWithEscape* '"'
    | '\'' (~'\'' | '\\\'') * '\''
    ;
fragment
StringCharWithEscape
    : ~["\\\r\n]
    | EscapeSequence
    ;
fragment
EscapeSequence
    : '\\' [btnfr"'\\]
//  | OctalEscape
//  | UnicodeEscape
    ;

// BOOLEAN
Boolean
    : 'true'
    | 'false'
    ;

// IDENTIFIER
Identifier : ( Letter | '_' ) ( Letter | '_' | [0-9] )* ;
fragment
Lowercase
    : [a-z]
    ;
fragment
Uppercase
    : [A-Z]
    ;
fragment
Letter
    : Lowercase
    | Uppercase
    ;

fragment
OptNeg
    : '-'?
    ;
fragment
Number
    : [1-9] [0-9]*
    ;

// DOUBLE
Double
    : DoubleZero
    | OptNeg DoubleNonzero
    ;
fragment
DoubleSuffix
    : 'D'
    | 'd'
    ;
fragment
DoubleZero
    : '0' DoubleSuffix
    | '0.0' DoubleSuffix?
    ;
fragment
DoubleNonzero
    : Number DoubleSuffix
    | '0'? '.' [0-9]+ DoubleSuffix?
    | Number '.' [0-9]+ DoubleSuffix?
    ;

// FLOAT
Float
    : FloatZero
    | OptNeg FloatNonzero
    ;
fragment
FloatSuffix
    : 'F'
    | 'f'
    ;
fragment
FloatZero
    : '0' FloatSuffix
    | '0.0' FloatSuffix
    ;
fragment
FloatNonzero
    : Number FloatSuffix
    | '0'? '.' [0-9]+ FloatSuffix
    | Number '.' [0-9]+ FloatSuffix
    ;

// LONG
Long
    : '0' LongSuffix
    | OptNeg Number LongSuffix
    ;
fragment
LongSuffix
    : 'L'
    | 'l'
    ;

// SHORT
Short
    : '0' ShortSuffix
    | OptNeg Number ShortSuffix
    ;
fragment
ShortSuffix
    : 'S'
    | 's'
    ;

// BYTE
Byte
    : '0' ByteSuffix
    | OptNeg Number ByteSuffix
    ;
fragment
ByteSuffix
    : 'B'
    | 'b'
    ;

// INTEGER
Integer
    : '0'
    | OptNeg Number
    ;

Comment
    : '//' ~[\r\n]* -> skip
    ;

Whitespace
    : [ \t\r\n\u000C]+ -> skip
    ;