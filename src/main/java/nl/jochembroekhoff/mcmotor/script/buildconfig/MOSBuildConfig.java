/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.buildconfig;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class MOSBuildConfig {

    /**
     * Set this to <code>true</code> in order to enable debug output.
     * Debug mode compared to production mode:
     * <ul>
     * <li>Uses clear scoreboard names. Production mode uses random identifiers and prepends them with a <code>#</code>,
     * which hides the values when the scoreboard is displayed in e.g. the sidebar.</li>
     * <li>In the future, debug mode may also add comments to the generated files where the line numbers from the source
     * code would be placed.</li>
     * </ul>
     */
    @SerializedName("debug_mode")
    private boolean debugMode = false;

    private String namespace = "motorscript";

    private List<String> inputs = Collections.emptyList();

    @SerializedName("output_folder")
    private String outputFolder = "output";

    private MCMeta mcmeta = new MCMeta();

    public static MOSBuildConfig defaultConfig() {
        return new MOSBuildConfig();
    }
}
