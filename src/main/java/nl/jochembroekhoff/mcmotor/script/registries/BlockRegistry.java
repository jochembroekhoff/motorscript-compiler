/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.registries;

import com.google.gson.reflect.TypeToken;
import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.report.BlocksReportItem;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class BlockRegistry {

    private static final Map<String, Map<String, BlocksReportItem>> registeredBlocks = new HashMap<>();

    public static void load() {
        Map<String, BlocksReportItem> report = Main.GSON.fromJson(
                new InputStreamReader(BlockRegistry.class.getResourceAsStream("/reports/blocks.json")),
                TypeToken.getParameterized(Map.class, String.class, BlocksReportItem.class).getType());

        report.forEach((fullIdentifier, reportItem) -> {
            val split = fullIdentifier.split(":", 2);
            val namespace = split[0];
            val path = split[1];

            if (!registeredBlocks.containsKey(namespace)) {
                registeredBlocks.put(namespace, new HashMap<>());
            }

            registeredBlocks.get(namespace).put(path, reportItem);
        });
    }

    public static BlocksReportItem get(RegistryItem registryItem) {
        if (registryItem == null) {
            return null;
        }

        val namespace = registryItem.getNamespace();
        val path = registryItem.getPath();

        if (!registeredBlocks.containsKey(namespace)) {
            return null;
        }

        return registeredBlocks.get(namespace).getOrDefault(path, null);
    }
}
