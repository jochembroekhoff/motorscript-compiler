/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.registries;

import com.google.gson.reflect.TypeToken;
import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.report.GlobalRegistryItem;

import java.io.InputStreamReader;
import java.util.*;

public class GlobalRegistry {

    private static final Map<String, Map<String, List<RegistryItem>>> registeredItems = new HashMap<>();

    public static void load() {
        Map<String, GlobalRegistryItem> report = Main.GSON.fromJson(
                new InputStreamReader(GlobalRegistry.class.getResourceAsStream("/reports/registries.json")),
                TypeToken.getParameterized(Map.class, String.class, GlobalRegistryItem.class).getType());

        report.forEach((fullIdentifier, registryItem) -> {
            val split = fullIdentifier.split(":", 2);
            val namespace = split[0];
            val path = split[1];

            if (!registeredItems.containsKey(namespace)) {
                registeredItems.put(namespace, new HashMap<>());
            }

            if (!registeredItems.get(namespace).containsKey(path)) {
                registeredItems.get(namespace).put(path, new ArrayList<>(registryItem.getEntries().size()));
            }

            val list = registeredItems.get(namespace).get(path);

            registryItem.getEntries().forEach((fullIdentifier_, registryEntry) -> {
                val spl = fullIdentifier_.split(":", 2);
                list.add(new RegistryItem<>(spl[0], spl[1], null));
            });
        });
    }

    public static List<RegistryItem> getRegistryItems(String namespace, String path) {
        if (namespace == null || path == null) {
            return Collections.emptyList();
        }

        if (!registeredItems.containsKey(namespace)) {
            return Collections.emptyList();
        }

        return registeredItems.get(namespace).getOrDefault(path, Collections.emptyList());
    }

    public static boolean isRegistered(List<RegistryItem> items, String namespace, String path) {
        if (items == null) return false;

        for (val item : items) {
            if (item.getNamespace().equals(namespace) && item.getPath().equals(path)) {
                return true;
            }
        }

        return false;
    }

    public static RegistryItem getFromIndex(List<RegistryItem> items, String namespace, String path) {
        if (items == null) return null;

        for (val item : items) {
            if (item.getNamespace().equals(namespace) && item.getPath().equals(path)) {
                return item;
            }
        }

        return null;
    }
}
