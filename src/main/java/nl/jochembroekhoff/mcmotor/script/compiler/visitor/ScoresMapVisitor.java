/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;

import java.util.HashMap;
import java.util.Map;

public class ScoresMapVisitor extends MOSBaseVisitorWithScope<Map<String, Range<Integer>>> {
    ScoresMapVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Map<String, Range<Integer>> visitScoresMap(MOSParser.ScoresMapContext ctx) {
        var output = new HashMap<String, Range<Integer>>();

        for (var kv : ctx.scoresMapKeyValue()) {
            var key = kv.Identifier().getText();
            var rangeVisitor = new RangeVisitor(getScope());
            var range = rangeVisitor.visit(kv.range());
            if (range.getClazz() != Integer.class) {
                System.err.printf("Cannot add Range of type %s to a scores map. A scores map requires ranges of type Double.\n", range.getClazz().getSimpleName());
            } else {
                output.put(key, range);
            }
        }

        return output;
    }
}
