/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import net.querz.nbt.Tag;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NBT extends MOSType {

    final Tag value;

    {
        setStaticCapabilities(
                MOSTypeCapability.FindByIndex,
                MOSTypeCapability.FindByString,
                MOSTypeCapability.GetCompileTime
        );
    }

    @Override
    public boolean checkDynamicCapability(MOSTypeCapability capability) {
        switch (capability) {
            case GetRunTime:
                return NBTTools.isNumeric(value.getType());
            default:
                return false;
        }
    }

    @Override
    public MOSResult<NBT> find(int index) {
        return new MOSResult<>(true, new NBT(NBTTools.retrieveValueFromTag(value, index)));
    }

    @Override
    public MOSResult<NBT> find(String segment) {
        return new MOSResult<>(true, new NBT(NBTTools.retrieveValueFromTag(value, segment)));
    }

    @Override
    public MOSResult<NBT> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public MOSResult<Variable> getRunTime(Scope runtimeScope) {
        var variable = new Variable(runtimeScope, NBTTools.getIntegerValue(value));
        return new MOSResult<>(true, variable);
    }
}
