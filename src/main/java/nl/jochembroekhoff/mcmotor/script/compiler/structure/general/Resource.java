/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.var;
import net.querz.nbt.CompoundTag;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Resource extends MOSType implements IProvidesMinecraftForm {
    final String namespace;
    final String path;
    final IProperties properties;
    final CompoundTag payload;

    {
        setStaticCapabilities(MOSTypeCapability.GetCompileTime);
    }

    @Override
    public String toString() {
        return getMinecraftForm();
    }

    @Override
    public MOSResult<Resource> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public String getMinecraftForm() {
        return getMinecraftForm(false);
    }

    public String getMinecraftForm(boolean namespaceAndPathOnly) {
        var result = namespace + ":" + path;

        if (namespaceAndPathOnly) {
            return result;
        }

        if (properties instanceof IProvidesMinecraftForm) {
            result += "[";
            result += ((IProvidesMinecraftForm) properties).getMinecraftForm();
            result += "]";
        }

        // Make sure to NOT include the payload if it is empty.
        // Appending an empty compound value ({}) to a resource generally makes it useless.
        if (payload != null && payload.size() > 0) {
            result += payload.toTagString();
        }

        return result;
    }
}
