/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.writer;

import com.google.gson.stream.JsonReader;
import lombok.val;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IInstructionWithSourcePosition;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction._NewScope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.TagConfiguration;

import java.io.*;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ResultWriter {
    public static void write(File dataRoot, Scope globalScope) {
        val dataRootCurrentNamespace = new File(dataRoot, Main.BUILD_CONFIG.getNamespace());
        dataRootCurrentNamespace.mkdirs();

        val functionsRoot = new File(dataRootCurrentNamespace, "functions");
        functionsRoot.mkdirs();

        val internalFunctionsDir = new File(functionsRoot, "_internal");
        internalFunctionsDir.mkdirs();

        val scopeOutputFolder = new File(internalFunctionsDir, Main.CURRENT_ENTRYPOINT);
        scopeOutputFolder.mkdirs();

        val mainEntrypoint = writeScope(scopeOutputFolder, globalScope);

        try (val mainFos = new FileOutputStream(new File(functionsRoot, Main.CURRENT_ENTRYPOINT + ".mcfunction")); val pw = new PrintWriter(mainFos)) {
            // Header
            pw.println("## MotorScript Compiler Export ##");
            pw.println("## Description: Main function for entrypoint " + Main.CURRENT_ENTRYPOINT);
            pw.print("## Date: ");
            pw.print(LocalDateTime.now(Clock.systemUTC()));
            pw.println(" UTC ##");

            // Call global scope
            pw.print("function ");
            pw.print(Main.BUILD_CONFIG.getNamespace());
            pw.print(":_internal/");
            pw.print(Main.CURRENT_ENTRYPOINT);
            pw.print('/');
            pw.println(mainEntrypoint);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (var kv : globalScope.getCompilerInstructions().entrySet()) {
            switch (kv.getKey()) {
                case "tags":
                    for (var value : kv.getValue()) {
                        if (value instanceof Resource) {
                            var resource = (Resource) value;
                            addFunctionToTag(dataRoot, resource.getNamespace(), resource.getPath(), Main.BUILD_CONFIG.getNamespace() + ":" + Main.CURRENT_ENTRYPOINT);
                        }
                    }
                    break;
            }
        }
    }

    public static void writeSetupFunction(File dataRoot) {
        val setupFunctionFile = new File(dataRoot, Main.BUILD_CONFIG.getNamespace() + File.separator + "functions" + File.separator + "_internal" + File.separator + "_setup.mcfunction");
        val setupFunctionReference = Main.BUILD_CONFIG.getNamespace() + ":_internal/_setup";

        setupFunctionFile.getParentFile().mkdirs();

        try (val setupFos = new FileOutputStream(setupFunctionFile); val pw = new PrintWriter(setupFos)) {
            pw.println("## MotorScript Compiler Export ##");
            pw.print("## Description: Scoreboard setup for namespace ");
            pw.print(Main.BUILD_CONFIG.getNamespace());
            pw.println(" ##");
            pw.print("## Date: ");
            pw.print(LocalDateTime.now(Clock.systemUTC()));
            pw.println(" UTC ##");

            pw.print("scoreboard objectives add ");
            pw.print(Main.BUILD_CONFIG.getNamespace());
            pw.println(" dummy");
        } catch (IOException e) {
            e.printStackTrace();
        }

        addFunctionToTag(dataRoot, "minecraft", "load", setupFunctionReference);
    }

    private static String writeScope(File outputDir, Scope scope) {
        try (var fos = new FileOutputStream(new File(outputDir, scope.getId() + ".mcfunction")); var pw = new PrintWriter(fos)) {
            for (var instruction : scope.getInstructions()) {
                // Write the source position if debug mode is enabled
                if (Main.BUILD_CONFIG.isDebugMode() && instruction instanceof IInstructionWithSourcePosition) {
                    var sourcePosition = ((IInstructionWithSourcePosition) instruction).getSourcePosition();
                    pw.printf("## source line=%d, column=%d\n", sourcePosition.getLine(), sourcePosition.getColumn());
                }

                if (instruction instanceof IMinecraftInstruction) {
                    var mcInstruction = (IMinecraftInstruction) instruction;
                    pw.println(mcInstruction.toMinecraftCommand());
                } else {
                    if (instruction instanceof _NewScope) {
                        var _enterScope = (_NewScope) instruction;
                        writeScope(outputDir, _enterScope.getTargetScope());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return scope.getId();
    }

    private static boolean addFunctionToTag(File datapackDataRoot, String tagNamespace, String tagName, String fullFunctionName) {
        var functionsTagsRoot = new File(datapackDataRoot, tagNamespace + File.separatorChar + "tags" + File.separatorChar + "functions");
        functionsTagsRoot.mkdirs();
        var tagFile = new File(functionsTagsRoot, tagName + ".json");

        System.out.printf("Add function %s to tag %s.\n", fullFunctionName, tagName);

        TagConfiguration tagConfiguration;
        try (var reader = new JsonReader(new FileReader(tagFile))) {
            tagConfiguration = Main.GSON_PRETTY.fromJson(reader, TagConfiguration.class);
        } catch (IOException e) {
            tagConfiguration = new TagConfiguration();
            tagConfiguration.setValues(new ArrayList<>());
        }

        if (!tagConfiguration.getValues().contains(fullFunctionName)) {
            tagConfiguration.getValues().add(fullFunctionName);
        }

        try (Writer writer = new FileWriter(tagFile)) {
            Main.GSON_PRETTY.toJson(tagConfiguration, writer);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
