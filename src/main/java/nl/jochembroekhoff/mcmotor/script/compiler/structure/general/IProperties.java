/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general;

import nl.jochembroekhoff.mcmotor.script.compiler.visitor.ExpressionVisitor;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;

import java.util.HashMap;
import java.util.Map;

public interface IProperties {
    /**
     * Add or set a property to this properties container.
     * <p>
     * Note: {@code value} is a raw type produced by the {@link ExpressionVisitor}.
     *
     * @param key    property key
     * @param negate if the value is negated
     * @param value  the value
     * @return Whether the property has been added or set successfully.
     */
    boolean setOrAddProperty(String key, boolean negate, MOSType value);

    /**
     * Construct a new default instance of an {@link IProperties}.
     * This instance implements the {@link IProperties} in the most basic way: all keys are valid and negations are
     * ignored.
     */
    static IProperties newDefaultPropertiesContainer() {
        return new IProperties() {

            private Map<String, MOSType> values = new HashMap<>();

            @Override
            public boolean setOrAddProperty(String key, boolean negated, MOSType value) {
                values.put(key, value);
                return true;
            }
        };
    }
}
