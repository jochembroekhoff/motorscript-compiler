/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import nl.jochembroekhoff.mcmotor.script.type.defaults.Void;

import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SelectorTagMutation extends MOSType {

    private final Selector target;
    private final String mutation;
    private static final String[] invocationSignature = {"tag_name"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        var transformed = transformArgumentsFromSignature(invocationSignature, argumentsUnnamed, argumentsNamed);

        var tag_name = CompileTimeTools.extractString(transformed.get("tag_name"));

        if (tag_name == null) {
            return new MOSResult<>(false, null);
        }

        if (tag_name.isEmpty()) {
            System.err.println("The tag name value must not be empty.");
            return new MOSResult<>(false, null);
        }

        scope.getInstructions().add((IMinecraftInstruction) () -> "tag " + target.getMinecraftForm() + " " + mutation + " " + tag_name);

        return new MOSResult<>(true, new Void());
    }
}
