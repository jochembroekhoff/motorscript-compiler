/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import net.querz.nbt.CompoundTag;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExperienceQuery extends MOSType {

    final Selector target;
    final String unit;

    {
        setStaticCapabilities(MOSTypeCapability.GetRunTime);
    }

    @Override
    public MOSResult<Variable> getRunTime(Scope runtimeScope) {
        // TODO: Do not modify the original selector arguments
        // Querying XP on a target implies that it is a player.

        target.getSelectorProperties().setType(new Resource("minecraft", "player", IProperties.newDefaultPropertiesContainer(), new CompoundTag("")));
        if (!target.getAt().isSingular()) {
            target.getSelectorProperties().setLimit(1);
        }

        var resultVariable = new Variable();

        runtimeScope.getInstructions().add((IMinecraftInstruction) () -> {
            var sb = new StringBuilder();
            sb.append("execute store result score ");
            sb.append(resultVariable.getId());
            sb.append(' ');
            sb.append(Main.BUILD_CONFIG.getNamespace());
            sb.append(" run experience query ");
            sb.append(target.getMinecraftForm());
            sb.append(' ');
            sb.append(unit);
            return sb.toString();
        });

        return new MOSResult<>(true, resultVariable);
    }
}
