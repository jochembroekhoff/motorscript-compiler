/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.SelectorTarget;

public class SelectorVisitor extends MOSBaseVisitorWithScope<Selector> {
    SelectorVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Selector visitSelector(MOSParser.SelectorContext ctx) {
        Selector selector = new Selector();

        if (ctx.selectorDefault() != null) {
            switch (ctx.selectorDefault().SelectorDefault().getText().substring(1)) {
                case "p":
                    selector.setAt(SelectorTarget.NEAREST_PLAYER);
                    break;
                case "r":
                    selector.setAt(SelectorTarget.RANDOM_PLAYER);
                    break;
                case "a":
                    selector.setAt(SelectorTarget.ALL_PLAYERS);
                    break;
                case "e":
                    selector.setAt(SelectorTarget.ALL_ENTITIES);
                    break;
                case "s":
                    selector.setAt(SelectorTarget.EXECUTING_ENTITY);
                    break;
            }
        } else if (ctx.selectorEntityName() != null) {
            selector.setAt(SelectorTarget.ALL_ENTITIES);
            selector.getSelectorProperties().setName(ctx.selectorEntityName().Identifier().getText());
        }

        if (ctx.properties() != null) {
            var propertiesVisitor = new PropertiesVisitor(getScope(), selector.getSelectorProperties());
            propertiesVisitor.visit(ctx.properties());
        }

        return selector;
    }
}
