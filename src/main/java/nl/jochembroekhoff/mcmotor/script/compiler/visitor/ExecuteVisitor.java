/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.CallScope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.ExecuteChain;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.*;

public class ExecuteVisitor extends MOSBaseVisitorWithScope<Object> {

    ExecuteVisitor(Scope initialScope) {
        super(initialScope);
    }

    private ExecuteChain executeChain = new ExecuteChain();

    @Override
    public Object visitExecuteAs(MOSParser.ExecuteAsContext ctx) {
        var selectorVisitor = new SelectorVisitor(getScope());
        var selector = selectorVisitor.visit(ctx.selector());
        executeChain.addChainPart(new ExecuteAs(selector));

        return getScope();
    }

    @Override
    public Object visitExecuteAt(MOSParser.ExecuteAtContext ctx) {
        var selectorVisitor = new SelectorVisitor(getScope());
        var selector = selectorVisitor.visit(ctx.selector());
        executeChain.addChainPart(new ExecuteAt(selector));

        return getScope();
    }

    /*
    @Override
    public Object visitExecuteFacing(MOSParser.ExecuteFacingContext ctx) {
        var positionVisitor = new PositionVisitor(getScope());
        var position = positionVisitor.visit(ctx.position());
        executeChain.addChainPart(new ExecuteFacingPosition(position));

        return getScope();
    }
    */

    @Override
    public Object visitExecuteIn(MOSParser.ExecuteInContext ctx) {
        var resourceVisitor = new ResourceVisitor(getScope());
        var resource = resourceVisitor.visit(ctx.resource());
        executeChain.addChainPart(new ExecuteIn(resource));

        return getScope();
    }

    @Override
    public Object visitExecutePositioned(MOSParser.ExecutePositionedContext ctx) {
        if (ctx.As() != null) {
            var selectorVisitor = new SelectorVisitor(getScope());
            var selector = selectorVisitor.visit(ctx.selector());
            executeChain.addChainPart(new ExecutePositionedAs(selector));
        } else {
            var positionVisitor = new PositionVisitor(getScope());
            var position = positionVisitor.visit(ctx.position());
            executeChain.addChainPart(new ExecutePositioned(position));
        }

        return getScope();
    }

    @Override
    public Object visitBlock(MOSParser.BlockContext ctx) {
        pushScope(false);
        {
            StatementVisitor.visitAllStatements(ctx.statement(), getScope());
        }
        var executeBodyScope = popScope();

        executeChain.setRunInstruction(new CallScope(executeBodyScope));
        getScope().getInstructions().add(executeChain);

        return getScope();
    }
}
