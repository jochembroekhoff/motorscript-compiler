/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.val;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Position;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.position.PositionPartType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.TypeTools;

public class PositionVisitor extends MOSBaseVisitorWithScope<Position> {
    PositionVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Position visitPosition(MOSParser.PositionContext ctx) {
        var partX = ctx.positionPart(0);
        var partY = ctx.positionPart(1);
        var partZ = ctx.positionPart(2);

        var position = new Position();
        var expressionVisitor = new ExpressionVisitor(getScope());

        MOSParser.ExpressionContext expressionContextX;
        MOSParser.ExpressionContext expressionContextY;
        MOSParser.ExpressionContext expressionContextZ;

        if (partX.expression() != null) {
            expressionContextX = partX.expression();
            position.setXType(PositionPartType.EXACT);
        } else {
            if (partX.positionOffsetTilde() != null) {
                expressionContextX = partX.positionOffsetTilde().expression();
                position.setXType(PositionPartType.TILDE);
            } else {
                expressionContextX = partX.positionOffsetCaret().expression();
                position.setXType(PositionPartType.CARET);
            }
        }
        if (partY.expression() != null) {
            expressionContextY = partY.expression();
            position.setYType(PositionPartType.EXACT);
        } else {
            if (partY.positionOffsetTilde() != null) {
                expressionContextY = partY.positionOffsetTilde().expression();
                position.setYType(PositionPartType.TILDE);
            } else {
                expressionContextY = partY.positionOffsetCaret().expression();
                position.setYType(PositionPartType.CARET);
            }
        }
        if (partZ.expression() != null) {
            expressionContextZ = partZ.expression();
            position.setZType(PositionPartType.EXACT);
        } else {
            if (partZ.positionOffsetTilde() != null) {
                expressionContextZ = partZ.positionOffsetTilde().expression();
                position.setZType(PositionPartType.TILDE);
            } else {
                expressionContextZ = partZ.positionOffsetCaret().expression();
                position.setZType(PositionPartType.CARET);
            }
        }

        if (expressionContextX != null) {
            val expressionX = TypeTools.performGetCompileTime(expressionVisitor.visit(expressionContextX));
            if (!(expressionX instanceof LiteralValue)) {
                System.err.printf("Cannot create a Position with an X coordinate of type %s.\n", expressionX.getClass().getSimpleName());
                return null;
            }

            val literal = (LiteralValue) expressionX;
            if (!literal.getType().isNumeric()) {
                System.err.printf("Cannot create a Position with a literal value of type %s.\n", literal.getType());
                return null;
            }

            position.setX(((Number) literal.getValue()).doubleValue());
        } else {
            position.setX(0);
        }

        if (expressionContextY != null) {
            val expressionY = TypeTools.performGetCompileTime(expressionVisitor.visit(expressionContextY));
            if (!(expressionY instanceof LiteralValue)) {
                System.err.printf("Cannot create a Position with an Y coordinate of type %s.\n", expressionY.getClass().getSimpleName());
                return null;
            }

            val literal = (LiteralValue) expressionY;
            if (!literal.getType().isNumeric()) {
                System.err.printf("Cannot create a Position with a literal value of type %s.\n", literal.getType());
                return null;
            }

            position.setY(((Number) literal.getValue()).doubleValue());
        } else {
            position.setY(0);
        }

        if (expressionContextZ != null) {
            val expressionZ = TypeTools.performGetCompileTime(expressionVisitor.visit(expressionContextZ));
            if (!(expressionZ instanceof LiteralValue)) {
                System.err.printf("Cannot create a Position with an Z coordinate of type %s.\n", expressionZ.getClass().getSimpleName());
                return null;
            }

            val literal = (LiteralValue) expressionZ;
            if (!literal.getType().isNumeric()) {
                System.err.printf("Cannot create a Position with a literal value of type %s.\n", literal.getType());
                return null;
            }

            position.setZ(((Number) literal.getValue()).doubleValue());
        } else {
            position.setZ(0);
        }

        return position;
    }
}
