/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteChainPart;

import java.util.ArrayList;
import java.util.List;

public class ExecuteChain implements IMinecraftInstruction {

    @Setter
    private IMinecraftInstruction runInstruction = null;

    @Getter
    private List<IExecuteChainPart> chainParts = new ArrayList<>();

    public void addChainPart(IExecuteChainPart chainPart) {
        chainParts.add(chainPart);
    }

    @Override
    public String toMinecraftCommand() {
        val builder = new StringBuilder();

        builder.append("execute ");

        for (val chainPart : chainParts) {
            val minecraftForm = chainPart.toExecuteChainPartString();
            builder.append(minecraftForm);

            // Only append a space if the provided Minecraft form is not empty
            if (!minecraftForm.isEmpty()) {
                builder.append(' ');
            }
        }

        if (runInstruction != null) {
            builder.append("run ");
            builder.append(runInstruction.toMinecraftCommand());
        }

        return builder.toString();
    }
}
