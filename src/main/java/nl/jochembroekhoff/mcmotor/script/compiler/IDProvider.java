/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler;

import lombok.val;
import lombok.var;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class IDProvider {

    private static final Map<String, Map<String, String>> allProvidedIds = new HashMap<>();

    private static final Object lock = new Object();

    private static final Map<String, Integer> counters = new ConcurrentHashMap<>();

    public static String next() {
        return next("__internal__");
    }

    public static String next(Object object) {
        return next(object.getClass());
    }

    public static String next(Class clazz) {
        return next(clazz.getSimpleName());
    }

    public static String next(String prefix) {
        return next(Main.CURRENT_ENTRYPOINT, prefix, true);
    }

    public static String next(String group, String prefix, boolean addCounterValue) {
        // Minecraft only accepts lowercase
        var result = group.toLowerCase() + "__" + prefix.toLowerCase();

        if (addCounterValue) {

            synchronized (lock) {
                if (!counters.containsKey(prefix))
                    counters.put(prefix, -1);
            }

            var next = counters.get(prefix) + 1;
            counters.put(prefix, next);

            result += next;
        }

        if (Main.BUILD_CONFIG.isDebugMode()) {
            return result;
        }

        if (!allProvidedIds.containsKey(group)) {
            allProvidedIds.put(group, new HashMap<>());
        }

        val groupContainer = allProvidedIds.get(group);

        if (groupContainer.containsKey(result)) {
            return groupContainer.get(result);
        }

        val uuid = UUID.randomUUID().toString();

        groupContainer.put(result, uuid);

        return uuid;
    }

    /**
     * The {@link IDProvider} will be reset automatically for every input.
     */
    static void reset() {
        synchronized (lock) {
            counters.clear();
        }
    }
}
