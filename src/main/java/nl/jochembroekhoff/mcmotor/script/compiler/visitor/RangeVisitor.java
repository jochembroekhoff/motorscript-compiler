/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.range.RangeType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

public class RangeVisitor extends MOSBaseVisitorWithScope<Range> {
    RangeVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Range visitRangeExact(MOSParser.RangeExactContext ctx) {
        var literalExact = getLiteralValueFromRangePart(ctx.rangePart(), getScope());

        Range range;
        if (literalExact.getType().isDecimal()) {
            var rangeTmp = Range.newRange(Double.class);
            rangeTmp.setExact(((Number) literalExact.getValue()).doubleValue());
            range = rangeTmp;
        } else if (literalExact.getType().isNumeric()) {
            var rangeTmp = Range.newRange(Integer.class);
            rangeTmp.setExact(((Number) literalExact.getValue()).intValue());
            range = rangeTmp;
        } else {
            System.err.printf("Cannot create a range from the literal value of type %s.\n", literalExact.getType());
            return null;
        }

        range.setType(RangeType.EXACT);
        return range;
    }

    @Override
    public Range visitRangeFromTo(MOSParser.RangeFromToContext ctx) {
        var literalFrom = getLiteralValueFromRangePart(ctx.rangePart(0), getScope());
        var literalTo = getLiteralValueFromRangePart(ctx.rangePart(1), getScope());

        Range range;

        if (literalFrom.getType().isDecimal() || literalTo.getType().isDecimal()) {
            var rangeTmp = Range.newRange(Double.class);
            rangeTmp.setFrom(((Number) literalFrom.getValue()).doubleValue());
            rangeTmp.setTo(((Number) literalTo.getValue()).doubleValue());
            range = rangeTmp;
        } else {
            var rangeTmp = Range.newRange(Integer.class);
            rangeTmp.setFrom(((Number) literalFrom.getValue()).intValue());
            rangeTmp.setTo(((Number) literalTo.getValue()).intValue());
            range = rangeTmp;
        }

        range.setType(RangeType.FROM_TO);
        return range;
    }

    @Override
    public Range visitRangeOrBelow(MOSParser.RangeOrBelowContext ctx) {
        var literalBelow = getLiteralValueFromRangePart(ctx.rangePart(), getScope());

        Range range;
        if (literalBelow.getType().isDecimal()) {
            var rangeTmp = Range.newRange(Double.class);
            rangeTmp.setBelow(((Number) literalBelow.getValue()).doubleValue());
            range = rangeTmp;
        } else {
            var rangeTmp = Range.newRange(Integer.class);
            rangeTmp.setBelow(((Number) literalBelow.getValue()).intValue());
            range = rangeTmp;
        }

        range.setType(RangeType.BELOW);
        return range;
    }

    @Override
    public Range visitRangeOrAbove(MOSParser.RangeOrAboveContext ctx) {
        var literalAbove = getLiteralValueFromRangePart(ctx.rangePart(), getScope());

        Range range;
        if (literalAbove.getType().isDecimal()) {
            var rangeTmp = Range.newRange(Double.class);
            rangeTmp.setAbove(((Number) literalAbove.getValue()).doubleValue());
            range = rangeTmp;
        } else {
            var rangeTmp = Range.newRange(Integer.class);
            rangeTmp.setAbove(((Number) literalAbove.getValue()).intValue());
            range = rangeTmp;
        }

        range.setType(RangeType.ABOVE);
        return range;
    }

    /**
     * Get a LiteralValue containing the value represented by the given {@link MOSParser.RangePartContext}.
     * <p>
     * It does not matter if the provided {@link MOSParser.RangePartContext} directly contains a {@link LiteralValue} or
     * if it contains a constant reference to a {@link LiteralValue}.
     *
     * @param ctx   The {@link MOSParser.RangePartContext} to extract a {@link LiteralValue} from.
     * @param scope The current {@link Scope} to work in.
     * @return
     */
    private static LiteralValue getLiteralValueFromRangePart(MOSParser.RangePartContext ctx, Scope scope) {
        if (ctx.literalNumeric() != null) {
            var literalVisitor = new LiteralVisitor(scope);
            return literalVisitor.visit(ctx.literalNumeric());
        } else if (ctx.reference() != null) {
            var referenceVisitor = new ReferenceVisitor(scope);
            var reference = referenceVisitor.visit(ctx.reference());

            if (!reference.hasCapability(MOSTypeCapability.GetCompileTime)) {
                System.err.println("Reference used in range does not have the 'GetCompiletime' capability.");
                return null;
            }

            var getResult = reference.getCompileTime();
            var get = getResult.getValue();

            if (get instanceof LiteralValue) {
                return (LiteralValue) get;
            }

            if (get instanceof NBT) {
                var tag = ((NBT) get).getValue();
                LiteralValue literalValue = null;

                if (NBTTools.isValidInteger(tag)) {
                    literalValue = new LiteralValue(LiteralType.INTEGER);
                    literalValue.setValue(NBTTools.getIntegerValue(tag));
                } else if (NBTTools.isValidDouble(tag)) {
                    literalValue = new LiteralValue(LiteralType.DOUBLE);
                    literalValue.setValue(NBTTools.getDoubleValue(tag));
                }

                if (literalValue != null) return literalValue;
            }

            System.err.printf("Unable to get a valid range value from the reference type %s.\n", get.getClass().getSimpleName());
        }

        return null;
    }
}
