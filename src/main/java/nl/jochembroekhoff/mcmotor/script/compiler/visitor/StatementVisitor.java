/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.val;
import lombok.var;
import net.querz.nbt.*;
import nl.jochembroekhoff.mcmotor.script.compiler.IDProvider;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.Constant;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.range.RangeType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.properties.DeclarationProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.AssignVariableToVariable;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.CallScope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.ExecuteChain;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalScoreMatches;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.LinkedList;

public class StatementVisitor extends MOSBaseVisitorWithScope<Scope> {

    /**
     * Defines the IDProvider prefix to be used for assignment of persistent variables.
     */
    private static final String IDPROVIDER_PREFIX_ASGNPSTRACK = "asgn_pstrack";

    /**
     * Defines the IDProvider group to be used for shared variables.
     */
    private static final String IDPROVIDER_GROUP_INTERNAL_VARMODESHARED = "__vms__";

    /**
     * Defines the IDProvider group to be used for the asgn_pstrack values of shared variables.
     */
    private static final String IDPROVIDER_GROUP_INTERNAL_VARMODESHARED_ASGNPSTRACK = "__vms_ap__";

    public StatementVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Scope visitDeclarationStatement(MOSParser.DeclarationStatementContext ctx) {
        val declarationProperties = new DeclarationProperties();
        val sourceName = ctx.Identifier().getText();

        if (ctx.annotation() != null) {
            new PropertiesVisitor(getScope(), declarationProperties).visit(ctx.annotation().properties());
        }
        if (ctx.Const() != null) {
            var expressionVisitor = new ExpressionVisitor(getScope());
            var typeContainer = expressionVisitor.visit(ctx.expression());
            var constant = new Constant(typeContainer);
            constant.setSourceName(ctx.Identifier().getText());
            getScope().introduceNewConstant(constant);
        } else {
            var expressionVisitor = new ExpressionVisitor(getScope());
            var expression = expressionVisitor.visit(ctx.expression());

            if (!expression.hasCapability(MOSTypeCapability.GetRunTime)) {
                System.err.printf("The type %s does not have the 'GetRunTime' capability.\n", expression.getClass().getSimpleName());
                return null;
            }

            var getResult = expression.getRunTime(getScope());
            var runtimeValue = getResult.getValue();

            if (!getResult.isSuccess()) {
                System.err.printf("Invoking 'GetRunTime' on %s returned false.\n", expression.getClass().getSimpleName());
                return null;
            }

            if (declarationProperties.hasMode("persistent")) {
                final Variable persistentStateTrack;
                final Variable persistentVariable;

                if (declarationProperties.hasMode("shared")) {
                    persistentStateTrack = new Variable(IDProvider.next(IDPROVIDER_GROUP_INTERNAL_VARMODESHARED_ASGNPSTRACK, sourceName, false));
                    persistentVariable = new Variable(IDProvider.next(IDPROVIDER_GROUP_INTERNAL_VARMODESHARED, sourceName, false));
                } else {
                    persistentStateTrack = new Variable(IDProvider.next(IDPROVIDER_PREFIX_ASGNPSTRACK));
                    persistentVariable = new Variable();
                }

                persistentVariable.setSourceName(sourceName);

                // Prepare the initialization scope
                pushScope(false);
                {
                    getScope().getInstructions().add(new AssignVariableToVariable(persistentVariable, runtimeValue));
                    getScope().getInstructions().add((IMinecraftInstruction) () -> "scoreboard players set " + persistentStateTrack.getMinecraftForm() + " 1");
                }
                var poppedScope = popScope();

                // Initialize the value if no priming value is set
                var executeChain = new ExecuteChain();
                executeChain.addChainPart(new ExecuteConditionalScoreMatches(true, persistentStateTrack, Range.one(Integer.class)));
                executeChain.setRunInstruction(new CallScope(poppedScope));

                getScope().getInstructions().add(executeChain);
                getScope().introduceNewVariable(persistentVariable);
            } else {
                var clonedVariable = new Variable(runtimeValue);
                clonedVariable.setSourceName(ctx.Identifier().getText());
                getScope().introduceNewVariable(clonedVariable);
            }
        }

        return getScope();
    }

    @Override
    public Scope visitAssignStatement(MOSParser.AssignStatementContext ctx) {

        var referenceVisitor = new ReferenceVisitor(getScope());
        var reference = referenceVisitor.visit(ctx.reference());

        var expressionVisitor = new ExpressionVisitor(getScope());
        var expression = expressionVisitor.visit(ctx.expression());

        if (ctx.assignment().Assign() != null) { // Regular assignment
            if (reference.hasCapability(MOSTypeCapability.Assign)) {
                if (!reference.assign(getScope(), expression)) {
                    System.err.println("Error while compiling a reference assign statement. Returned false.");
                }
            } else {
                System.err.printf("The reference of type %s lacks the 'Assign' capability.\n", reference.getClass().getSimpleName());
            }
        } else { // Operation assignment (assign-with-operation)
            var operator = "";

            if (ctx.assignment().AssignMultiply() != null) {
                operator = "*";
            } else if (ctx.assignment().AssignDivide() != null) {
                operator = "/";
            } else if (ctx.assignment().AssignModulus() != null) {
                operator = "%";
            } else if (ctx.assignment().AssignPlus() != null) {
                operator = "+";
            } else if (ctx.assignment().AssignMinus() != null) {
                operator = "-";
            }

            if (reference.hasCapability(MOSTypeCapability.AssignWithOperation)) {
                if (!reference.assignWithOperation(getScope(), operator, expression)) {
                    System.err.println("Error while compiling a reference assign-with-operation statement. Returned false.");
                }
            } else {
                System.err.printf("The reference of type %s lacks the 'AssignWithOperation' capability.\n", reference.getClass().getSimpleName());
            }
        }

        return getScope();
    }

    @Override
    public Scope visitForInfinite(MOSParser.ForInfiniteContext ctx) {
        pushScope();
        {
            visitAllStatements(ctx.block().statement(), getScope());
            getScope().getInstructions().add(new CallScope(getScope()));
        }
        popScope();

        return getScope();
    }

    @Override
    public Scope visitForIn(MOSParser.ForInContext ctx) {
        var providedConstantFirst = ctx.Identifier(0).getText();
        var providedConstantSecond = (String) null;
        if (ctx.Identifier(1) != null)
            providedConstantSecond = ctx.Identifier(1).getText();

        var expressionContext = ctx.forExpression();

        Range range = null;
        Tag nbt = null;

        if (expressionContext.reference() != null) {
            var referenceVisitor = new ReferenceVisitor(getScope());
            var reference = referenceVisitor.visit(expressionContext.reference());
            throw new UnsupportedOperationException("For in reference is TODO."); // TODO
        } else if (expressionContext.range() != null) {
            var rangeVisitor = new RangeVisitor(getScope());
            range = rangeVisitor.visit(expressionContext.range());
        } else if (expressionContext.nbt() != null) {
            var nbtVisitor = new NBTVisitor(getScope());
            nbt = nbtVisitor.visit(expressionContext.nbt());
        }

        var iteratingKeyConstants = new LinkedList<Constant>();
        var iteratingValueConstants = new LinkedList<Constant>();

        if (range != null) {
            // TODO: Also add step support (default to something like 1)
            if (!(Integer.class.isAssignableFrom(range.getClazz()))) {
                System.err.printf("Cannot do a for loop with a range containing %s. Only Integer allowed.\n", range.getClazz().getSimpleName());
                return null;
            }
            if (range.getType() != RangeType.FROM_TO) {
                System.err.printf("Cannot do a for loop with a range of type %s. Only FROM_TO allowed.\n", range.getType());
                return null;
            }

            Range<Integer> rangeInt = range.convertTo(Integer.class);

            for (int i = rangeInt.getFrom(); i <= rangeInt.getTo(); i++) {
                val literal = new LiteralValue(LiteralType.INTEGER);
                literal.setValue(i);
                iteratingValueConstants.add(new Constant(literal));
            }
        } else if (nbt != null) {
            switch (nbt.getType()) {
                case COMPOUND:
                    if (providedConstantSecond == null) {
                        System.out.println("Warning: for-in loop with only one provided constant while iterating NBT. Only iterating values.");
                    }
                    var nbtCompound = (CompoundTag) nbt;
                    for (var kv : nbtCompound.getValue().entrySet()) {
                        var keyLiteral = new LiteralValue(LiteralType.STRING);
                        keyLiteral.setValue(kv.getKey());
                        iteratingKeyConstants.add(new Constant(keyLiteral));
                        iteratingValueConstants.add(new Constant(new NBT(kv.getValue())));
                    }
                    break;
                case LONG_ARRAY:
                    var nbtLongArray = (LongArrayTag) nbt;
                    for (long l : nbtLongArray.getValue()) {
                        var literal = new LiteralValue(LiteralType.LONG);
                        literal.setValue(l);
                        iteratingValueConstants.add(new Constant(literal));
                    }
                    break;
                case INT_ARRAY:
                    var nbtIntArray = (IntArrayTag) nbt;
                    for (int i : nbtIntArray.getValue()) {
                        var literal = new LiteralValue(LiteralType.INTEGER);
                        literal.setValue(i);
                        iteratingValueConstants.add(new Constant(literal));
                    }
                    break;
                case BYTE_ARRAY:
                    var nbtByteArray = (ByteArrayTag) nbt;
                    for (byte b : nbtByteArray.getValue()) {
                        var literal = new LiteralValue(LiteralType.BYTE);
                        literal.setValue(b);
                        iteratingValueConstants.add(new Constant(literal));
                    }
                    break;
                case LIST:
                    var nbtList = (ListTag) nbt;
                    for (var listValue : nbtList.getValue()) {
                        iteratingValueConstants.add(new Constant(new NBT(listValue)));
                    }
                    break;
            }
        }

        if (providedConstantSecond != null) {
            if (iteratingKeyConstants.size() != iteratingValueConstants.size()) {
                System.err.println("Unable to iterate the for-in loop with key,value: the calculated key and value size do not match.");
                return null;
            }

            int i = 0;
            for (var valueConstant : iteratingValueConstants) {
                pushScope();
                {
                    var keyConstant = iteratingKeyConstants.get(i);

                    keyConstant.setSourceName(providedConstantFirst);
                    valueConstant.setSourceName(providedConstantSecond);

                    getScope().introduceNewConstant(keyConstant);
                    getScope().introduceNewConstant(valueConstant);

                    visitAllStatements(ctx.block().statement(), getScope());
                }
                popScope();

                i++;
            }
        } else {
            for (var valueConstant : iteratingValueConstants) {
                pushScope();
                {
                    valueConstant.setSourceName(providedConstantFirst);
                    getScope().introduceNewConstant(valueConstant);
                    visitAllStatements(ctx.block().statement(), getScope());
                }
                popScope();
            }
        }

        return getScope();
    }

    @Override
    public Scope visitIfStatement(MOSParser.IfStatementContext ctx) {
        Variable branchTracking = null;

        if (!ctx.ifStatementElseIfBlock().isEmpty() || ctx.ifStatementElseBlock() != null) {
            branchTracking = new Variable(getScope(), 0, "if_btrack");
        }

        processIfBranch(ctx.ifStatementFirstBlock().expression(), ctx.ifStatementFirstBlock().block(), branchTracking);

        for (var elseIfBranch : ctx.ifStatementElseIfBlock()) {
            processIfBranch(elseIfBranch.expression(), elseIfBranch.block(), branchTracking);
        }

        if (ctx.ifStatementElseBlock() != null) {
            processIfBranch(null, ctx.ifStatementElseBlock().block(), branchTracking);
        }

        return getScope();
    }

    /**
     * Processes a branch of an if statement.
     *
     * @param expressionContext Conditional expression to evaluate. If <code>null</code>, no check will be performed.
     * @param blockContext      Block to run when the given expression results in a positive result.
     * @param branchTracking    {@link Variable} in which branch tracking values are stored.
     *                          If set to <code>null</code>, no branch tracking will be performed.
     */
    private void processIfBranch(MOSParser.ExpressionContext expressionContext, MOSParser.BlockContext blockContext, Variable branchTracking) {
        var doBranchTracking = branchTracking != null;
        var executeChain = new ExecuteChain();

        if (doBranchTracking) {
            executeChain.addChainPart(new ExecuteConditionalScoreMatches(false, branchTracking, Range.zero(Integer.class)));
        }

        if (expressionContext != null) {
            var expressionVisitor = new ExpressionVisitor(getScope());
            var expression = expressionVisitor.visit(expressionContext);

            if (!expression.hasCapability(MOSTypeCapability.ProvideExecuteChainPart)) {
                System.err.printf("The type %s does not have the 'ProvideExecuteChainPart' capability.\n", expression.getClass().getSimpleName());
                return;
            }

            var executeChainPartProvisionResult = expression.provideExecuteChainPart(getScope());
            if (!executeChainPartProvisionResult.isSuccess()) {
                System.err.printf("The type %s did not provide a successful result for invocation of the 'ProvideExecuteChainPart' capability.\n", expression.getClass().getSimpleName());
                return;
            }

            executeChain.addChainPart(executeChainPartProvisionResult.getValue());
        }

        pushScope(false);
        {
            if (doBranchTracking) {
                getScope().getInstructions().add((IMinecraftInstruction) () -> "scoreboard players add " + branchTracking.getId() + " " + Main.BUILD_CONFIG.getNamespace() + " 1");
            }

            visitAllStatements(blockContext.statement(), getScope());
        }
        var conditionalBody = popScope();

        executeChain.setRunInstruction(new CallScope(conditionalBody));

        getScope().getInstructions().add(executeChain);
    }

    @Override
    public Scope visitExecuteStatement(MOSParser.ExecuteStatementContext ctx) {
        var executeVisitor = new ExecuteVisitor(getScope());

        executeVisitor.visit(ctx);

        return getScope();
    }

    @Override
    public Scope visitCallStatement(MOSParser.CallStatementContext ctx) {
        if (!getScope().isUnsafe()) {
            System.err.println("Call statement not allowed here: this scope is not unsafe.");
            return null;
        }

        var expressionVisitor = new ExpressionVisitor(getScope());
        var expression = expressionVisitor.visit(ctx.expression());

        if (!expression.hasCapability(MOSTypeCapability.GetCompileTime)) {
            System.err.printf("The type %s lacks the 'GetCompileTime' capability.\n", expression.getClass().getSimpleName());
            return null;
        }

        var getCompileTime = expression.getCompileTime();

        if (!getCompileTime.isSuccess()) {
            System.err.printf("Cannot compile a call statement. Invoking 'GetCompileTime' on %s returned false.\n", expression.getClass().getSimpleName());
            return null;
        }

        var compileTimeType = getCompileTime.getValue();

        if (compileTimeType instanceof Resource) {
            var resource = (Resource) compileTimeType;
            getScope().getInstructions().add((IMinecraftInstruction) () -> "function " + resource.getNamespace() + ":" + resource.getPath());
        } else {
            System.err.printf("The type %s is not an instance of Resource so it cannot be used as the argument for a call statement.\n", compileTimeType.getClass().getSimpleName());
            return null;
        }

        return getScope();
    }

    @Override
    public Scope visitScheduleStatement(MOSParser.ScheduleStatementContext ctx) {
        val delayType = new ExpressionVisitor(getScope()).visit(ctx.expression());
        val delay = CompileTimeTools.extractWhole(delayType, 1, Long.MAX_VALUE).longValue();

        pushScope(false);
        {
            visitAllStatements(ctx.block().statement(), getScope());
        }
        val scopeToSchedule = popScope();

        getScope().getInstructions().add((IMinecraftInstruction) () -> "schedule function " + scopeToSchedule.getResourceString() + " " + delay);

        return getScope();
    }

    @Override
    public Scope visitRawStatement(MOSParser.RawStatementContext ctx) {
        if (!getScope().isUnsafe()) {
            System.err.println("Raw statement not allowed here: this scope is not unsafe.");
            return null;
        }

        var rawCommandString = new StringBuilder();

        for (var expressionContext : ctx.expression()) {
            var expressionVisitor = new ExpressionVisitor(getScope());
            var expression = expressionVisitor.visit(expressionContext);

            if (!expression.hasCapability(MOSTypeCapability.GetCompileTime)) {
                System.err.printf("Cannot use this type in a raw statement: the type %s lacks the 'GetCompileTime' capability.\n", expression.getClass().getSimpleName());
                continue;
            }

            var getCompileTimeResult = expression.getCompileTime();
            var getCompileTimeValue = getCompileTimeResult.getValue();

            if (!getCompileTimeResult.isSuccess()) {
                continue;
            }

            if (getCompileTimeValue.hasCapability(MOSTypeCapability.ToConstantString)) {
                var toConstantStringResult = getCompileTimeValue.toConstantString();

                if (!toConstantStringResult.isSuccess()) {
                    System.err.printf("Cannot use this type in a raw statement: invoking 'ToConstantString' on %s returned false.\n", getCompileTimeValue.getClass().getSimpleName());
                    continue;
                }

                rawCommandString.append(toConstantStringResult.getValue());
            } else if (getCompileTimeValue instanceof IProvidesMinecraftForm) {
                rawCommandString.append(((IProvidesMinecraftForm) getCompileTimeValue).getMinecraftForm());
            } else {
                System.err.printf("Cannot use this type in a raw statement: the type %s lacks the 'ToConstantString' nor is a Minecraft form provider.\n", getCompileTimeValue.getClass().getSimpleName());
            }
        }

        getScope().getInstructions().add((IMinecraftInstruction) () -> rawCommandString.toString());

        return getScope();
    }

    @Override
    public Scope visitReference(MOSParser.ReferenceContext ctx) {
        var referenceVisitor = new ReferenceVisitor(getScope());
        var reference = referenceVisitor.visit(ctx);

        // TODO: Throw error if last reference part is not "invocation"

        return getScope();
    }

    public static void visitAllStatements(Iterable<MOSParser.StatementContext> statementContexts, Scope scope) {
        for (var statementContext : statementContexts) {
            var visitor = new StatementVisitor(scope);
            visitor.visit(statementContext);
        }
    }
}
