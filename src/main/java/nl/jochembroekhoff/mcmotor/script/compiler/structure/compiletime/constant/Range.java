/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.range.RangeType;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@EqualsAndHashCode(callSuper = true)
public class Range<T extends Number> extends MOSType {

    final Class<T> clazz;

    RangeType type;

    T exact;

    T from;
    T to;

    T above;

    T below;

    private Range(Class<T> clazz) {
        this.clazz = clazz;
    }

    {
        setStaticCapabilities(MOSTypeCapability.GetCompileTime);
    }

    public static <T extends Number> Range<T> newRange(Class<T> clazz) {
        return new Range<>(clazz);
    }

    @SuppressWarnings("unchecked")
    public <TT extends Number> Range<TT> convertTo(Class<TT> clazz) {

        if (this.clazz == clazz) return (Range<TT>) this;

        var newRange = newRange(clazz);
        newRange.setType(getType());

        if (Double.class.isAssignableFrom(clazz)) {
            if (exact != null) {
                newRange.setExact((TT) (Double) exact.doubleValue());
            }

            if (from != null && to != null) {
                newRange.setFrom((TT) (Double) from.doubleValue());
                newRange.setTo((TT) (Double) to.doubleValue());
            }

            if (above != null) {
                newRange.setAbove((TT) (Double) above.doubleValue());
            }

            if (below != null) {
                newRange.setBelow((TT) (Double) below.doubleValue());
            }
        } else if (Integer.class.isAssignableFrom(clazz)) {
            if (exact != null) {
                newRange.setExact((TT) (Integer) exact.intValue());
            }

            if (from != null && to != null) {
                newRange.setFrom((TT) (Integer) from.intValue());
                newRange.setTo((TT) (Integer) to.intValue());
            }

            if (above != null) {
                newRange.setAbove((TT) (Integer) above.intValue());
            }

            if (below != null) {
                newRange.setBelow((TT) (Integer) below.intValue());
            }
        } else {
            System.err.printf("Cannot convert Range of type %s to Range of type %s.\n", this.clazz.getSimpleName(), clazz.getSimpleName());
            return null;
        }

        return newRange;
    }

    @SuppressWarnings("unchecked")
    public static <TT extends Number> Range<TT> zero(Class<TT> clazz) {
        var range = newRange(clazz);
        range.setType(RangeType.EXACT);
        if (Double.class.isAssignableFrom(clazz)) {
            range.setExact((TT) (Double) 0d);
        } else if (Integer.class.isAssignableFrom(clazz)) {
            range.setExact((TT) (Integer) 0);
        } else {
            System.err.printf("Cannot create a zero Range of type %s.\n", clazz.getSimpleName());
            return null;
        }
        return range;
    }

    @SuppressWarnings("unchecked")
    public static <TT extends Number> Range<TT> one(Class<TT> clazz) {
        var range = newRange(clazz);
        range.setType(RangeType.EXACT);
        if (Double.class.isAssignableFrom(clazz)) {
            range.setExact((TT) (Double) 1d);
        } else if (Integer.class.isAssignableFrom(clazz)) {
            range.setExact((TT) (Integer) 1);
        } else {
            System.err.printf("Cannot create a one Range of type %s.\n", clazz.getSimpleName());
            return null;
        }
        return range;
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();

        switch (type) {
            case ABOVE:
                sb.append(above);
                sb.append("..");
                break;
            case BELOW:
                sb.append("..");
                sb.append(below);
                break;
            case EXACT:
                sb.append(exact);
                break;
            case FROM_TO:
                sb.append(from);
                sb.append("..");
                sb.append(to);
                break;
        }

        return sb.toString();
    }

    @Override
    public MOSResult<Range<T>> getCompileTime() {
        return new MOSResult<>(true, this);
    }
}
