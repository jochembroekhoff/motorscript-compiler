/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SelectorTag extends MOSType {

    private final Selector target;

    {
        setStaticCapabilities(MOSTypeCapability.FindByString);
    }

    @Override
    public MOSResult<? extends MOSType> find(String segment) {
        switch (segment) {
            case "add":
                return new MOSResult<>(true, new SelectorTagMutation(getTarget(), "add"));
            case "remove":
                return new MOSResult<>(true, new SelectorTagMutation(getTarget(), "remove"));
            default:
                return new MOSResult<>(false, null);
        }
    }
}
