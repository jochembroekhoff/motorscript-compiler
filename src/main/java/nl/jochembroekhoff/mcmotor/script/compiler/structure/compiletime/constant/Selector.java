/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.SelectorProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.SelectorTarget;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type.Experience;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type.SelectorScore;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type.SelectorTag;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalSelector;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Selector extends MOSType implements IProvidesMinecraftForm {

    private SelectorTarget at = SelectorTarget.ALL_ENTITIES;

    private final SelectorProperties selectorProperties = new SelectorProperties();

    {
        setStaticCapabilities(
                MOSTypeCapability.GetCompileTime,
                MOSTypeCapability.FindByString,
                MOSTypeCapability.Invocation,
                MOSTypeCapability.ProvideExecuteChainPart);
    }

    @Override
    public MOSResult<Selector> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public MOSResult<? extends MOSType> find(String segment) {
        switch (segment) {
            case "xp":
            case "experience":
                return new MOSResult<>(true, new Experience(this));
            case "score":
                return new MOSResult<>(true, new SelectorScore(this));
            case "tag":
                return new MOSResult<>(true, new SelectorTag(this));
        }

        return new MOSResult<>(false, null);
    }

    @Override
    public MOSResult<ExecuteConditionalSelector> provideExecuteChainPart(Scope runtimeScope) {
        return new MOSResult<>(true, new ExecuteConditionalSelector(false, this));
    }

    /**
     * Convert this selector to a string representation.
     * The value is in valid Minecraft form.
     *
     * @return String value in the format that is accepted by Minecraft.
     */
    public String getMinecraftForm() {
        var output = "@" + at.getMinecraftForm();
        var propertiesMinecraftForm = getSelectorProperties().getMinecraftForm();
        if (!propertiesMinecraftForm.isEmpty()) {
            output += "[" + propertiesMinecraftForm + "]";
        }
        return output;
    }

}
