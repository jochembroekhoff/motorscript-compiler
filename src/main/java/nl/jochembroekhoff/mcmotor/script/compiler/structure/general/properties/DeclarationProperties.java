/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general.properties;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;

import java.util.HashSet;
import java.util.Set;

public class DeclarationProperties implements IProperties {

    private Set<String> mode = new HashSet<>();

    @Override
    public boolean setOrAddProperty(String key, boolean negate, MOSType value) {
        if ("mode".equals(key)) {
            var modeValue = CompileTimeTools.extractString(value);
            if (modeValue != null) {
                if (negate) {
                    mode.remove(modeValue);
                } else {
                    mode.add(modeValue);
                }
                return true;
            }
        }

        return false;
    }

    public boolean hasMode(String modeQuery) {
        return mode.contains(modeQuery);
    }
}
