/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.position.PositionPartType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.EqualityOperator;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalBlock;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.TypeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import nl.jochembroekhoff.mcmotor.script.type.defaults.BoolRT;

import java.util.Locale;

@Data
@EqualsAndHashCode(callSuper = true)
public class Position extends MOSType implements IProvidesMinecraftForm {

    double x = 0;
    double y = 0;
    double z = 0;

    PositionPartType xType = PositionPartType.EXACT;
    PositionPartType yType = PositionPartType.EXACT;
    PositionPartType zType = PositionPartType.EXACT;

    {
        setStaticCapabilities(MOSTypeCapability.GetCompileTime, MOSTypeCapability.Compare);
    }

    @Override
    public MOSResult<Position> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public MOSResult<BoolRT> compare(Scope runtimeScope, EqualityOperator equalityOperator, MOSType valueToCompare) {
        if (equalityOperator != EqualityOperator.EqualTo && equalityOperator != EqualityOperator.NotEqualTo) {
            System.err.printf("Comparing Position to a different type is not possible with the following equality operator: %s.\n", equalityOperator);
            return new MOSResult<>(false, null);
        }

        val compileTimeValue = TypeTools.performGetCompileTime(valueToCompare);

        if (!(compileTimeValue instanceof Resource)) {
            System.err.printf("A Position cannot be compared with the type %s.\n", valueToCompare.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        val negate = equalityOperator == EqualityOperator.NotEqualTo;

        var resource = (Resource) compileTimeValue;
        return new MOSResult<>(true, new BoolRT(new ExecuteConditionalBlock(negate, this, resource)));
    }

    public String getMinecraftForm(boolean round) {
        val sb = new StringBuilder();

        val format = round
                ? "%.0f"
                : "%f";

        sb.append(xType.getPrefix());
        sb.append(String.format(Locale.ROOT, format, x));
        sb.append(' ');

        sb.append(yType.getPrefix());
        sb.append(String.format(Locale.ROOT, format, y));
        sb.append(' ');

        sb.append(zType.getPrefix());
        sb.append(String.format(Locale.ROOT, format, z));

        return sb.toString();
    }

    @Override
    public String getMinecraftForm() {
        return getMinecraftForm(false);
    }
}
