/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal;

import lombok.Getter;

public enum LiteralType {
    STRING(String.class),

    BOOLEAN(Boolean.class, true, true, false),

    DOUBLE(Double.class, true, false, true),
    FLOAT(Float.class, true, false, true),

    LONG(Long.class, true, true, false),
    INTEGER(Integer.class, true, true, false),
    SHORT(Short.class, true, true, false),
    BYTE(Byte.class, true, true, false);

    @Getter
    private final Class valueType;

    @Getter
    private final boolean isNumeric;

    @Getter
    private final boolean isWhole;

    @Getter
    private final boolean isDecimal;

    LiteralType(Class valueType, boolean isNumeric, boolean isWhole, boolean isDecimal) {
        this.valueType = valueType;
        this.isNumeric = isNumeric;
        this.isWhole = isWhole;
        this.isDecimal = isDecimal;
    }

    LiteralType(Class valueType) {
        this(valueType, false, false, false);
    }
}
