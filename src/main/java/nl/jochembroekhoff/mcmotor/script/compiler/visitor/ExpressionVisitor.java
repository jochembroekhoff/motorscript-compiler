/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.IDProvider;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.range.RangeType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.EqualityOperator;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.ExecuteChain;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.InitializeVariable;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalScoreMatches;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import nl.jochembroekhoff.mcmotor.script.type.defaults.BoolRT;

public class ExpressionVisitor extends MOSBaseVisitorWithScope<MOSType> {
    public ExpressionVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public MOSType visitExpression(MOSParser.ExpressionContext ctx) {
        if (ctx.expressionMulDivMod() != null || ctx.expressionPlusMinus() != null) {
            var expressionVisitorLeft = new ExpressionVisitor(getScope());
            var expressionLeft = expressionVisitorLeft.visit(ctx.expression(0));
            var expressionVisotorRight = new ExpressionVisitor(getScope());
            var expressionRight = expressionVisotorRight.visit(ctx.expression(1));

            if (!expressionLeft.hasCapability(MOSTypeCapability.Operation)) {
                System.err.printf("The type %s does not have the 'Operation' capability.\n", expressionLeft.getClass().getSimpleName());
                return null;
            }

            String operator = "";

            if (ctx.expressionMulDivMod() != null) {
                if (ctx.expressionMulDivMod().Multiply() != null) {
                    operator = "*";
                } else if (ctx.expressionMulDivMod().FSlash() != null) {
                    operator = "/";
                } else if (ctx.expressionMulDivMod().Modulus() != null) {
                    operator = "%";
                }
            } else if (ctx.expressionPlusMinus() != null) {
                if (ctx.expressionPlusMinus().Plus() != null) {
                    operator = "+";
                } else if (ctx.expressionPlusMinus().Minus() != null) {
                    operator = "-";
                }
            }

            var operationResult = expressionLeft.operation(getScope(), operator, expressionRight);
            // TODO: Check result

            return operationResult.getValue();
        } else if (ctx.equalityOperator() != null) {
            var equalityOperator = EqualityOperator.reverseValue(ctx.equalityOperator().getText());

            var leftExpressionVisitor = new ExpressionVisitor(getScope());
            var leftExpression = leftExpressionVisitor.visit(ctx.expression(0));

            var rightExpressionVisitor = new ExpressionVisitor(getScope());
            var rightExpression = rightExpressionVisitor.visit(ctx.expression(1));

            if (!leftExpression.hasCapability(MOSTypeCapability.Compare)) {
                System.err.printf("The type %s lacks the 'Compare' capability.\n", leftExpression.getClass().getSimpleName());
                return null;
            }

            var compareResult = leftExpression.compare(getScope(), equalityOperator, rightExpression);

            if (!compareResult.isSuccess()) {
                System.err.printf("The type %s did not provide a successful result for invocation of the 'Compare' capability.\n", leftExpression.getClass().getSimpleName());
                return null;
            }

            return compareResult.getValue();
        } else if (!ctx.conditionalOr().isEmpty() || !ctx.conditionalAnd().isEmpty()) { // Conditional combination
            var isOr = !ctx.conditionalOr().isEmpty();

            var rangeZero = Range.zero(Integer.class);

            var rangeSuccess = Range.newRange(Integer.class);
            rangeSuccess.setType(RangeType.EXACT);

            if (isOr) {
                rangeSuccess.setExact(1);
            } else {
                rangeSuccess.setExact(ctx.expression().size());
            }

            var ifState = new Variable(IDProvider.next("cond_comb"));

            getScope().getInstructions().add(new InitializeVariable(ifState, 0));

            int expressionIndex = 0;
            for (var expressionContext : ctx.expression()) {
                var expressionVisitor = new ExpressionVisitor(getScope());
                var expression = expressionVisitor.visit(expressionContext);

                if (!expression.hasCapability(MOSTypeCapability.ProvideExecuteChainPart)) {
                    System.err.printf("The nr. %d right type %s in the conditional combination lacks the 'ProvideExecuteChainPart' capability.\n", expressionIndex, expression.getClass().getSimpleName());
                    continue;
                }

                var rightECPResult = expression.provideExecuteChainPart(getScope());

                if (!rightECPResult.isSuccess()) {
                    System.err.printf("The nr. %d right type %s in the conditional combination did not provide a successful result for invocation of the 'ProvideExecuteChainPart' capability.\n", expressionIndex, expression.getClass().getSimpleName());
                    continue;
                }

                var chain = new ExecuteChain();

                if (isOr) {
                    chain.addChainPart(new ExecuteConditionalScoreMatches(false, ifState, rangeZero));
                }

                chain.addChainPart(rightECPResult.getValue());
                chain.setRunInstruction(() -> "scoreboard players add " + ifState.getId() + " " + Main.BUILD_CONFIG.getNamespace() + " 1");

                getScope().getInstructions().add(chain);

                expressionIndex++;
            }

            return new BoolRT(new ExecuteConditionalScoreMatches(false, ifState, rangeSuccess));
        } else if (ctx.Exclam() != null) {
            var expression = new ExpressionVisitor(getScope()).visit(ctx.expression(0));

            if (!expression.hasCapability(MOSTypeCapability.Negate)) {
                System.err.printf("The value cannot be negated: the type %s lacks the 'Negate' capability.\n", expression.getClass().getSimpleName());
                return null;
            }

            var negateResult = expression.negate();

            if (!negateResult.isSuccess()) {
                System.err.printf("Invoking 'Negate' on %s returned false.\n", expression.getClass().getSimpleName());
                return null;
            }

            return negateResult.getValue();
        } else if (ctx.expression().size() >= 1) {
            return visit(ctx.expression(0));
        } else {
            return visitChildren(ctx);
        }
    }

    @Override
    public MOSType visitReference(MOSParser.ReferenceContext ctx) {
        var referenceVisitor = new ReferenceVisitor(getScope());
        return referenceVisitor.visit(ctx);
    }

    @Override
    public Selector visitSelector(MOSParser.SelectorContext ctx) {
        var selectorVisitor = new SelectorVisitor(getScope());
        return selectorVisitor.visit(ctx);
    }

    @Override
    public LiteralValue visitLiteralValue(MOSParser.LiteralValueContext ctx) {
        var literalValue = new LiteralVisitor(getScope());
        return literalValue.visit(ctx);
    }

    @Override
    public Range visitRange(MOSParser.RangeContext ctx) {
        var rangeVisitor = new RangeVisitor(getScope());
        return rangeVisitor.visit(ctx);
    }

    @Override
    public NBT visitNbt(MOSParser.NbtContext ctx) {
        var nbtVisitor = new NBTVisitor(getScope());
        var nbt = nbtVisitor.visit(ctx);
        return new NBT(nbt);
    }

    @Override
    public MOSType visitPath(MOSParser.PathContext ctx) {
        var pathVisitor = new PathVisitor(getScope());
        return pathVisitor.visit(ctx);
    }

    @Override
    public MOSType visitPosition(MOSParser.PositionContext ctx) {
        var positionVisitor = new PositionVisitor(getScope());
        return positionVisitor.visit(ctx);
    }

    @Override
    public MOSType visitResource(MOSParser.ResourceContext ctx) {
        var resourceVisitor = new ResourceVisitor(getScope());
        return resourceVisitor.visit(ctx);
    }
}
