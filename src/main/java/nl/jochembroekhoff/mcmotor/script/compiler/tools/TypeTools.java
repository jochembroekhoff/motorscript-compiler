/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.tools;

import lombok.val;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

public class TypeTools {
    /**
     * @param type
     * @return Type instance if successful, {@code null} otherwise.
     */
    public static MOSType performGetCompileTime(MOSType type) {
        return performGetCompileTime(type, true);
    }

    /**
     * @param type
     * @param warn
     * @return Type instance if successful, {@code null} otherwise.
     */
    public static MOSType performGetCompileTime(MOSType type, boolean warn) {
        if (type == null) {
            if (warn) {
                System.err.println("Cannot use a null type.");
            }
            return null;
        }

        if (!type.hasCapability(MOSTypeCapability.GetCompileTime)) {
            if (warn) {
                System.err.printf("The type %s lacks the 'GetCompileTime' capability.\n", type.getClass().getSimpleName());
            }
            return null;
        }

        val getCompileTime = type.getCompileTime();

        if (!getCompileTime.isSuccess()) {
            if (warn) {
                System.err.printf("Invoking 'GetCompileTime' on %s returned false.\n", type.getClass().getSimpleName());
            }
            return null;
        }

        return getCompileTime.getValue();
    }
}
