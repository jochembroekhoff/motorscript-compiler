/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.tools;

import lombok.var;
import net.querz.nbt.*;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Path;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.IndexSegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.KeySegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.Segment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;

public class NBTTools {
    public static Tag createTagFromLiteralValue(LiteralValue literalValue) {
        switch (literalValue.getType()) {
            case STRING:
                return new StringTag(literalValue.getValue().toString());
            case BOOLEAN:
                return new ByteTag((boolean) literalValue.getValue());
            case DOUBLE:
                return new DoubleTag((double) literalValue.getValue());
            case FLOAT:
                return new FloatTag((float) literalValue.getValue());
            case LONG:
                return new LongTag((long) literalValue.getValue());
            case INTEGER:
                return new IntTag((int) literalValue.getValue());
            case SHORT:
                return new ShortTag((short) literalValue.getValue());
            case BYTE:
                return new ByteTag((byte) literalValue.getValue());
        }

        return null;
    }

    public static Tag retrieveValueFromTag(Tag input, String key) {
        if (input.getType() != TagType.COMPOUND) {
            System.err.printf("Cannot access the NBT tag of type %s using the key %s.", input.getType(), key);
            return input;
        }
        return ((CompoundTag) input).get(key);
    }

    public static Tag retrieveValueFromTag(Tag input, int index) {
        int size;

        switch (input.getType()) {
            case LIST:
                size = ((ListTag) input).size();
                break;
            case BYTE_ARRAY:
            case INT_ARRAY:
            case LONG_ARRAY:
                size = ((ArrayTag) input).length();
                break;
            default:
                System.err.printf("Cannot access the NBT tag of type %s using the index %d. The tag type must be a list or an array.", input.getType(), index);
                size = -1;
                break;
        }

        if (size < 0) {
            return null;
        }

        var processedIndex = index;

        if (index < 0) {
            processedIndex = size + index;
        }

        if (processedIndex >= size) {
            System.err.printf("Invalid index for the calculated size. Size: %d, index: %d.\n", size, processedIndex);
            return null;
        }

        switch (input.getType()) {
            case LIST:
                return ((ListTag) input).get(processedIndex);
            case BYTE_ARRAY:
                return new ByteTag(((ByteArrayTag) input).getValue()[processedIndex]);
            case INT_ARRAY:
                return new IntTag(((IntArrayTag) input).getValue()[processedIndex]);
            case LONG_ARRAY:
                return new LongTag(((LongArrayTag) input).getValue()[processedIndex]);
        }

        return null;
    }

    public static Tag retrieveValueFromPath(Tag input, Path path) {
        var lastTag = input;
        for (var segment : path.getSegments()) {
            lastTag = retrieveValueFromPathSegment(lastTag, segment);
        }

        return lastTag;
    }

    public static Tag retrieveValueFromPathSegment(Tag input, Segment segment) {
        switch (segment.getType()) {
            case KEY:
                return retrieveValueFromTag(input, ((KeySegment) segment).getKey());
            case INDEX:
                return retrieveValueFromTag(input, ((IndexSegment) segment).getIndex());
        }

        return input;
    }

    public static boolean isNumeric(TagType tagType) {
        return isNumericDecimal(tagType) || isNumericWhole(tagType);
    }

    public static boolean isNumericDecimal(TagType tagType) {
        return tagType == TagType.DOUBLE || tagType == TagType.FLOAT;
    }

    public static boolean isNumericWhole(TagType tagType) {
        return tagType == TagType.LONG || tagType == TagType.INT || tagType == TagType.SHORT || tagType == TagType.BYTE;
    }

    public static boolean isValidInteger(Tag input, boolean allowAllNumeric) {
        if (input.getType() != TagType.LONG && input.getType() != TagType.INT && input.getType() != TagType.SHORT && input.getType() != TagType.BYTE) {
            if (allowAllNumeric) {
                if (input.getType() != TagType.FLOAT && input.getType() != TagType.DOUBLE) {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (input.getType() == TagType.LONG) {
            var longValue = ((LongTag) input).getValue();
            if (longValue > Integer.MAX_VALUE || longValue < Integer.MIN_VALUE) {
                return false;
            }
        }

        if (input.getType() == TagType.DOUBLE || input.getType() == TagType.FLOAT) {
            var doubleValue = ((Number) input.getValue()).doubleValue();
            if (doubleValue > Integer.MAX_VALUE || doubleValue < Integer.MIN_VALUE) {
                return false;
            }
        }

        return true;
    }

    public static boolean isValidInteger(Tag input) {
        return isValidInteger(input, false);
    }

    public static boolean isValidDouble(Tag input) {
        return isNumeric(input.getType());
    }

    public static Integer getIntegerValue(Tag input, boolean allowAllNumeric) {
        if (!isValidInteger(input, allowAllNumeric)) return null;

        Number number = (Number) input.getValue();
        if (number == null) return null;

        return number.intValue();
    }

    public static Integer getIntegerValue(Tag input) {
        return getIntegerValue(input, false);
    }

    public static Double getDoubleValue(Tag input) {
        if (!isValidDouble(input)) return null;

        Number number = (Number) input.getValue();
        if (number == null) return null;

        return number.doubleValue();
    }
}
