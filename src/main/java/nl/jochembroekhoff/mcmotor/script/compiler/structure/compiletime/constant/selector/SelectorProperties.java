/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector;

import lombok.Data;
import lombok.var;
import net.querz.nbt.CompoundTag;
import net.querz.nbt.TagType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.arguments.SortingType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Gamemode;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IPropertiesWithScoresMap;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
public class SelectorProperties implements IPropertiesWithScoresMap, IProvidesMinecraftForm {

    /* Selection by Position */

    Double x;
    Double y;
    Double z;

    Range<Double> distance;

    Double dx;
    Double dy;
    Double dz;

    /* Selection by Scoreboard Values */

    final Map<String, Range<Integer>> scores = new HashMap<>();

    final Map<String, Boolean> tags = new HashMap<>();

    String team;
    boolean negateTeam;

    /* Selection by Traits */

    Integer limit;

    SortingType sort;

    Range<Integer> level;

    Gamemode gamemode;

    String name;
    boolean negateName = false;

    Range<Double> x_rotation;
    Range<Double> y_rotation;

    Resource type;
    boolean negateType = false;

    /* Other selections */

    final Set<Pair<CompoundTag, Boolean>> nbt = new HashSet<>();

    // TODO: Support advancements

    @Override
    public boolean setOrAddProperty(String key, boolean negate, MOSType valueTypeContainer) {
        var ok = false;

        if (valueTypeContainer instanceof LiteralValue) {
            var lValue = ((LiteralValue) valueTypeContainer).getValue();
            var lValueType = ((LiteralValue) valueTypeContainer).getType();

            var stringValue = lValue.toString();

            if (lValueType.isNumeric()) {
                var numericValue = (Number) lValue;
                switch (key) {
                    case "x":
                        setX(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "y":
                        setY(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "z":
                        setZ(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "dx":
                        setDx(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "dy":
                        setDy(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "dz":
                        setDz(numericValue.doubleValue());
                        ok = true;
                        break;
                    case "limit":
                        setLimit(numericValue.intValue());
                        ok = true;
                        break;
                }
            }

            if (lValueType == LiteralType.STRING) {
                switch (key) {
                    case "tag":
                        getTags().put(stringValue, negate);
                        ok = true;
                        break;
                    case "team":
                        setTeam(stringValue);
                        setNegateTeam(negate);
                        ok = true;
                        break;
                    case "name":
                        setName(stringValue);
                        setNegateName(negate);
                        ok = true;
                        break;
                    case "sort":
                        try {
                            setSort(SortingType.reverseValue(stringValue));
                            ok = true;
                        } catch (IllegalArgumentException iae) {
                            System.err.printf("Could not find a sorting method for \"%s\".\n", stringValue);
                        }
                        break;
                    case "gamemode":
                        try {
                            setGamemode(Gamemode.reverseValue(stringValue));
                            ok = true;
                        } catch (IllegalArgumentException iae) {
                            System.err.printf("Could not find a gamemode for \"%s\".\n", stringValue);
                        }
                        break;
                }
            }
        }

        if (valueTypeContainer instanceof Resource) {
            if ("type".equals(key)) {
                setType((Resource) valueTypeContainer);
                setNegateType(negate);
                ok = true;
            }
        }

        if (valueTypeContainer instanceof NBT) {
            var tag = ((NBT) valueTypeContainer).getValue();
            if ("nbt".equals(key)) {
                if (tag.getType() == TagType.COMPOUND) {
                    nbt.add(Pair.of((CompoundTag) tag, negate));
                    ok = true;
                }
            }
        }

        if (valueTypeContainer instanceof Range) {
            var range = (Range) valueTypeContainer;

            if (Integer.class.isAssignableFrom(range.getClazz())) {
                var rangeInteger = range.convertTo(Integer.class);
                switch (key) {
                    case "level":
                        setLevel(rangeInteger);
                        ok = true;
                        break;
                }
            }

            // Use double values for everything else
            var rangeDouble = range.convertTo(Double.class);
            switch (key) {
                case "distance":
                    setDistance(rangeDouble);
                    ok = true;
                    break;
                case "x_rotation":
                    setX_rotation(rangeDouble);
                    ok = true;
                    break;
                case "y_rotation":
                    setY_rotation(rangeDouble);
                    ok = true;
                    break;
            }
        }

        if (!ok) {
            System.err.printf("Cannot set the%s property \"%s\" with a value of type %s as a valid selector property.\n", negate ? " negated" : "", key, valueTypeContainer.getClass().getSimpleName());
        }

        return ok;
    }

    @Override
    public boolean setOrAddProperty(String key, boolean negate, Map<String, Range<Integer>> value) {
        if (!key.equals("scores")) return false;

        for (var kv : value.entrySet()) {
            scores.put(kv.getKey(), kv.getValue());
        }

        return true;
    }

    @Override
    public String getMinecraftForm() {
        var sb = new StringBuilder();

        /* Selection by Position */

        if (x != null) {
            sb.append("x=");
            sb.append(x);
            sb.append(',');
        }
        if (y != null) {
            sb.append("y=");
            sb.append(y);
            sb.append(',');
        }
        if (z != null) {
            sb.append("z=");
            sb.append(z);
            sb.append(',');
        }

        if (distance != null) {
            sb.append("distance=");
            sb.append(distance.toString());
            sb.append(',');
        }

        if (dx != null) {
            sb.append("dx=");
            sb.append(dx);
            sb.append(',');
        }
        if (dy != null) {
            sb.append("dy=");
            sb.append(dy);
            sb.append(',');
        }
        if (dz != null) {
            sb.append("dz=");
            sb.append(dz);
            sb.append(',');
        }

        /* Selection by Scoreboard Values */

        if (!scores.isEmpty()) {
            sb.append("scores={");
            for (var entry : scores.entrySet()) {
                sb.append(entry.getKey());
                sb.append('=');
                sb.append(entry.getValue().toString());
                sb.append(',');
            }
            // Remove the last comma
            sb.deleteCharAt(sb.length() - 1);
            sb.append("},");
        }

        if (!tags.isEmpty()) {
            for (var tag : tags.entrySet()) {
                sb.append("tag=");
                if (tag.getValue()) sb.append('!');
                sb.append(tag.getKey());
                sb.append(',');
            }
        }

        if (team != null) {
            sb.append("team=");
            if (negateTeam) sb.append('!');
            sb.append(team);
            sb.append(',');
        }

        /* Selection by Traits */

        if (limit != null) {
            sb.append("limit=");
            sb.append(limit);
            sb.append(',');
        }

        if (sort != null) {
            sb.append("sort=");
            sb.append(sort.getMinecraftForm());
            sb.append(',');
        }

        if (level != null) {
            sb.append("level=");
            sb.append(level.toString());
            sb.append(',');
        }

        if (gamemode != null) {
            sb.append("gamemode=");
            sb.append(gamemode.getMinecraftForm());
            sb.append(',');
        }

        if (name != null) {
            sb.append("name=");
            if (negateName) sb.append('!');
            sb.append(name);
            sb.append(',');
        }

        if (x_rotation != null) {
            sb.append("x_rotation=");
            sb.append(x_rotation.toString());
            sb.append(',');
        }
        if (y_rotation != null) {
            sb.append("y_rotation=");
            sb.append(y_rotation.toString());
            sb.append(',');
        }

        if (type != null) {
            sb.append("type=");
            if (negateType) sb.append('!');
            sb.append(type);
            sb.append(',');
        }

        /* Other selections */

        if (!nbt.isEmpty()) {
            for (var nbtEntry : nbt) {
                sb.append("nbt=");
                if (nbtEntry.getRight()) sb.append('!'); // negate
                sb.append(nbtEntry.getLeft().toTagString());
                sb.append(',');
            }
        }

        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        return getMinecraftForm();
    }
}
