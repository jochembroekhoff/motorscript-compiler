/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure;

import lombok.Data;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.IDProvider;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.Constant;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.scope.VarConstItroducingResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
public class Scope {

    final String id = IDProvider.next(this);

    /**
     * Key: constant id.
     * <p>
     * Value: constant instance.
     */
    final Map<String, Constant> constants = new HashMap<>();

    /**
     * Key: variable id.
     * <p>
     * Value: variable instance.
     */
    final Map<String, Variable> variables = new HashMap<>();

    final List<IInstruction> instructions = new LinkedList<>();

    /**
     * Mapping of compiler instructions.
     * <p>
     * Note that the nested types in the list are already prepared by performing 'GetCompileTime'.
     */
    final Map<String, List<MOSType>> compilerInstructions = new HashMap<>();

    final Scope parentScope;

    public VarConstItroducingResult introduceNewConstant(Constant newConstant) {
        if (constants.containsKey(newConstant.getSourceName()) || variables.containsKey(newConstant.getSourceName())) {
            return VarConstItroducingResult.ERROR;
        }

        var result = isConstPresentInHierarchy(newConstant.getSourceName()) || isVarPresentInHierarchy(newConstant.getSourceName())
                ? VarConstItroducingResult.HIDES_OTHER
                : VarConstItroducingResult.SUCCESS;

        constants.put(newConstant.getSourceName(), newConstant);

        // TODO: Register constant globally when it is needed for runtime usage

        System.out.printf("Introduced new constant (%s) in %s with the name %s. \n",
                newConstant.getId(), this.getId(), newConstant.getSourceName());

        return result;
    }

    public VarConstItroducingResult introduceNewVariable(Variable newVariable) {
        if (constants.containsKey(newVariable.getSourceName()) || variables.containsKey(newVariable.getSourceName())) {
            return VarConstItroducingResult.ERROR;
        }

        var result = isConstPresentInHierarchy(newVariable.getSourceName()) || isVarPresentInHierarchy(newVariable.getSourceName())
                ? VarConstItroducingResult.HIDES_OTHER
                : VarConstItroducingResult.SUCCESS;

        variables.put(newVariable.getSourceName(), newVariable);

        System.out.printf("Introduced new variable (%s) in %s with the name %s.\n",
                newVariable.getId(), this.getId(), newVariable.getSourceName());

        return result;
    }

    public boolean isConstPresentInHierarchy(String sourceName) {
        if (constants.containsKey(sourceName))
            return true;

        if (parentScope != null && parentScope != this)
            return parentScope.isConstPresentInHierarchy(sourceName);

        return false;
    }

    public boolean isVarPresentInHierarchy(String sourceName) {
        if (variables.containsKey(sourceName))
            return true;

        if (parentScope != null)
            return parentScope.isVarPresentInHierarchy(sourceName);

        return false;
    }

    public Constant getConstantFromHierarchy(String sourceName) {
        if (constants.containsKey(sourceName))
            return constants.get(sourceName);

        if (parentScope != null && parentScope != this)
            return parentScope.getConstantFromHierarchy(sourceName);

        return null;
    }

    public Variable getVariableFromHierarchy(String sourceName) {
        if (variables.containsKey(sourceName))
            return variables.get(sourceName);

        if (parentScope != null && parentScope != this)
            return parentScope.getVariableFromHierarchy(sourceName);

        return null;
    }

    public boolean isUnsafe() {
        if (compilerInstructions.containsKey("unsafe")) {
            var unsafeInstructions = compilerInstructions.get("unsafe");
            if (unsafeInstructions.size() > 1) {
                System.out.println("Warning: more than one \"unsafe\" compiler instruction present.");
            }
            for (var instruction : unsafeInstructions) {
                if (instruction instanceof LiteralValue) {
                    var literalValue = (LiteralValue) instruction;

                    if (literalValue.getType() == LiteralType.BOOLEAN) {
                        return (Boolean) literalValue.getValue();
                    }
                }
            }
        }

        if (parentScope != null) {
            return parentScope.isUnsafe();
        }

        return false;
    }

    public String getResourceString() {
        return Main.BUILD_CONFIG.getNamespace() + ":_internal/" + Main.CURRENT_ENTRYPOINT + "/" + id;
    }

}
