/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.tools;

import lombok.val;
import org.apache.commons.text.StringEscapeUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTools {

    public static Pattern noNeedToEscapePattern = Pattern.compile("[A-z0-9_\\-+]+");

    public static String removeQuotationMarksAndUnescape(String input) {
        if (input == null) return null;

        if (input.length() <= 2) return "";

        val content = input.substring(1, input.length() - 1);

        if (input.startsWith("'") && input.endsWith("'")) {
            return content;
        }

        return StringEscapeUtils.unescapeJava(content);
    }

    public static String createPossiblyEscapedString(String input) {
        Matcher matcher = noNeedToEscapePattern.matcher(input);

        if (matcher.matches()) {
            return input;
        }

        return "\"" + StringEscapeUtils.escapeJava(input) + "\"";
    }

}
