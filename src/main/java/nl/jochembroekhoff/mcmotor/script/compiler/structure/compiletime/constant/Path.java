/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.Segment;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class Path extends MOSType {

    final List<Segment> segments;

    {
        setStaticCapabilities(MOSTypeCapability.GetCompileTime);
    }

    @Override
    public MOSResult<Path> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        for (var segment : segments) {
            sb.append(segment.toString());
        }
        return sb.toString();
    }

}
