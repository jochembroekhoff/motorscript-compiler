/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IPropertiesWithScoresMap;

public class PropertiesVisitor extends MOSBaseVisitorWithScope<IProperties> {

    private IProperties propertiesContainer;

    PropertiesVisitor(Scope initialScope, IProperties propertiesContainer) {
        super(initialScope);
        this.propertiesContainer = propertiesContainer;
    }

    @Override
    public IProperties visitPropertyKeyValue(MOSParser.PropertyKeyValueContext ctx) {
        var key = ctx.Identifier().getText();
        var negate = ctx.OpNotEqualTo() != null;

        if (ctx.scoresMap() != null) {
            var scoresMapVisitor = new ScoresMapVisitor(getScope());
            var scoresMap = scoresMapVisitor.visit(ctx.scoresMap());

            if (propertiesContainer instanceof IPropertiesWithScoresMap) {
                ((IPropertiesWithScoresMap) propertiesContainer).setOrAddProperty(key, negate, scoresMap);
            } else {
                System.err.printf("The properties container of type %s does not support score maps.\n", propertiesContainer.getClass().getSimpleName());
            }
        } else if (ctx.expression() != null) {
            var expressionVisitor = new ExpressionVisitor(getScope());
            var expression = expressionVisitor.visit(ctx.expression());

            if (!propertiesContainer.setOrAddProperty(key, negate, expression)) {
                System.err.printf("Failed to set the property value of %s to a value of type %s.\n", key, expression.getClass().getSimpleName());
            }
        }

        return propertiesContainer;
    }
}
