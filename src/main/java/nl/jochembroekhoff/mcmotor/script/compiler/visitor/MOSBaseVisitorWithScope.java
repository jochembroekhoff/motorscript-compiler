/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSBaseVisitor;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.CallScope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction._NewScope;

import java.util.concurrent.atomic.AtomicReference;

public class MOSBaseVisitorWithScope<T> extends MOSBaseVisitor<T> {

    private final AtomicReference<Scope> scopeRef = new AtomicReference<>();

    MOSBaseVisitorWithScope(Scope initialScope) {
        scopeRef.set(initialScope);
    }

    public Scope getScope() {
        return scopeRef.get();
    }

    /**
     * Enter a new child scope. The parent of the new scope is the current scope.
     *
     * @return The new child scope.
     */
    Scope pushScope(boolean autoEnter) {
        var parentScope = scopeRef.get();
        var newScope = new Scope(parentScope);

        parentScope.getInstructions().add(new _NewScope(newScope));
        if (autoEnter)
            parentScope.getInstructions().add(new CallScope(newScope));

        scopeRef.set(newScope);

        return newScope;
    }

    /**
     * AutoEnter is true.
     *
     * @return The new child scope.
     */
    Scope pushScope() {
        return pushScope(true);
    }

    /**
     * Leave the current scope and go back to the parent scope.
     *
     * @return The popped scope.
     */
    Scope popScope() {
        var scopeToPop = scopeRef.get();

        if (scopeToPop.getParentScope() != null) {
            scopeRef.set(scopeRef.get().getParentScope());
            return scopeToPop;
        }

        return null;
    }

}
