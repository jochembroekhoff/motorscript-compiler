/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.IndexSegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.KeySegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.Segment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.LiteralValueTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.StringTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

public class PathSegmentVisitor extends MOSBaseVisitorWithScope<Segment> {
    PathSegmentVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Segment visitPathSegmentIndex(MOSParser.PathSegmentIndexContext ctx) {
        if (ctx.Integer() != null) {
            return new IndexSegment(Integer.parseInt(ctx.Integer().getText()));
        }

        var expressionVisitor = new ExpressionVisitor(getScope());
        var expression = expressionVisitor.visit(ctx.expression());

        if (!expression.hasCapability(MOSTypeCapability.GetCompileTime)) {
            System.err.printf("The type %s in path segment does not have the 'GetCompileTime' capability.\n", expression.getClass().getSimpleName());
            return null;
        }

        var getResult = expression.getCompileTime();
        var container = getResult.getValue();

        if (container instanceof LiteralValue) {
            return new IndexSegment(LiteralValueTools.getIntegerValue((LiteralValue) container));
        }

        if (container instanceof NBT) {
            var tag = ((NBT) container).getValue();
            if (NBTTools.isValidInteger(tag)) {
                return new IndexSegment(NBTTools.getIntegerValue(tag));
            }
        }

        System.err.printf("Cannot create an index path segment of a reference of type %s.\n", expression.getClass().getSimpleName());
        return null;
    }

    @Override
    public Segment visitPathSegmentString(MOSParser.PathSegmentStringContext ctx) {
        if (ctx.Identifier() != null)
            return new KeySegment(ctx.Identifier().getText());

        if (ctx.String() != null)
            return new KeySegment(StringTools.removeQuotationMarksAndUnescape(ctx.String().getText()));

        return null;
    }
}
