/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moandjiezana.toml.Toml;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.buildconfig.MOSBuildConfig;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSLexer;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.visitor.ScriptVisitor;
import nl.jochembroekhoff.mcmotor.script.compiler.writer.MCMetaWriter;
import nl.jochembroekhoff.mcmotor.script.compiler.writer.ResultWriter;
import nl.jochembroekhoff.mcmotor.script.registries.BlockRegistry;
import nl.jochembroekhoff.mcmotor.script.registries.GlobalRegistry;
import nl.jochembroekhoff.mcmotor.script.type.InternalFunctionsManager;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Main {

    public final static Gson GSON_PRETTY = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    public final static Gson GSON = new GsonBuilder()
            .create();

    public static MOSBuildConfig BUILD_CONFIG = null;

    public static File WORKING_DIR = new File(".");
    public static File OUTPUT_DIR;

    public static String CURRENT_ENTRYPOINT;

    public static void main(String... args) throws IOException {
        /* Load build config */

        if (args.length == 1) {
            var buildConfigFile = new File(args[0]);
            var toml = new Toml().read(buildConfigFile);

            BUILD_CONFIG = toml.to(MOSBuildConfig.class);

            /* Prepare working dir */

            WORKING_DIR = buildConfigFile.getCanonicalFile().getParentFile();
            WORKING_DIR.mkdirs();
        } else {
            BUILD_CONFIG = MOSBuildConfig.defaultConfig();
        }

        if (BUILD_CONFIG.getNamespace().length() > 16) {
            System.err.printf("Namespace must be 16 or less characters long (currently %d). In the future this might change, but this is because of limitations of the Minecraft scoreboard system.\n", BUILD_CONFIG.getNamespace().length());
            System.exit(-1);
        }

        /* Initialize registries and managers */

        GlobalRegistry.load();
        BlockRegistry.load();

        InternalFunctionsManager.registerDefaults();

        /* Prepare output dir */

        OUTPUT_DIR = new File(WORKING_DIR, BUILD_CONFIG.getOutputFolder());
        OUTPUT_DIR.mkdirs();

        var dataDir = new File(OUTPUT_DIR, "data");
        dataDir.mkdirs();

        /* Compile all inputs */

        for (var input : BUILD_CONFIG.getInputs()) {
            System.out.println("==> Compiling: " + input);

            var inputFile = new File(WORKING_DIR, input + ".mos");
            CURRENT_ENTRYPOINT = FilenameUtils.getBaseName(inputFile.getName());

            /* TODO: Check if CURRENT_ENTRYPOINT is a valid Minecraft identifier. */

            if (CURRENT_ENTRYPOINT.startsWith("__") && CURRENT_ENTRYPOINT.endsWith("__")) {
                System.err.println("--> Input will produce an invalid entry point name: must not start and end with double underscore (__).");
            }

            IDProvider.reset();

            var sourceCode = CharStreams.fromStream(new FileInputStream(inputFile));

            var lexer = new MOSLexer(sourceCode);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new MOSParser(tokenStream);

            var scriptVisitor = new ScriptVisitor();
            scriptVisitor.visit(parser.script());

            var globalScope = scriptVisitor.getScope();
            ResultWriter.write(dataDir, globalScope);
        }

        System.out.println("==> Writing scoreboard setup function...");
        ResultWriter.writeSetupFunction(dataDir);

        /* Write datapack metadata if required */

        if (BUILD_CONFIG.getMcmeta().isCreate()) {
            System.out.println("==> Writing .mcmeta file...");
            MCMetaWriter.writeMCMeta();
        }

        System.out.println("==> Done. Output is located at " + OUTPUT_DIR.getCanonicalPath());
    }
}
