/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;

@Data
@RequiredArgsConstructor
public class ExecuteConditionalScoreMatches implements IExecuteConditional {

    final boolean negate;
    final Variable left;
    final Range right;

    @Override
    public String toExecuteChainPartString() {
        return new StringBuilder()
                .append(negate ? "unless" : "if")
                .append(" score ")

                .append(left.getMinecraftForm())

                .append(" matches ")

                .append(right.toString())

                .toString();
    }

    @Override
    public ExecuteConditionalScoreMatches duplicate(boolean negateDuplicate) {
        return new ExecuteConditionalScoreMatches(negateDuplicate, left, right);
    }
}
