/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import net.querz.nbt.*;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.StringTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.ArrayList;
import java.util.HashMap;

public class NBTVisitor extends MOSBaseVisitorWithScope<Tag> {

    public NBTVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public CompoundTag visitNbtCompound(MOSParser.NbtCompoundContext ctx) {
        var compound = new CompoundTag(new HashMap<>());

        for (var compoundKeyValueContext : ctx.nbtCompoundKeyValue()) {
            var keyContext = compoundKeyValueContext.nbtCompoundKey();

            var key = "";
            if (keyContext.Identifier() != null) {
                key = keyContext.Identifier().getText();
            } else if (keyContext.String() != null) {
                key = StringTools.removeQuotationMarksAndUnescape(keyContext.String().getText());
            }

            var nbtVisitor = new NBTVisitor(getScope());
            var valueTag = nbtVisitor.visit(compoundKeyValueContext.nbtCompoundValue().nbt());
            valueTag.setName(key);
            compound.set(valueTag);
        }

        return compound;
    }

    @Override
    public ArrayTag visitNbtArray(MOSParser.NbtArrayContext ctx) {
        var typeString = ctx.nbtArrayType().getText();
        LiteralType type = null;
        switch (typeString) {
            case "B":
                type = LiteralType.BYTE;
                break;
            case "I":
                type = LiteralType.INTEGER;
                break;
            case "L":
                type = LiteralType.LONG;
                break;
        }

        if (type == null) {
            System.err.println("Invalid array type indicator: " + typeString);
            return null;
        }

        var numValues = ctx.nbtArrayValue().size();
        var bytes = new byte[numValues];
        var ints = new int[numValues];
        var longs = new long[numValues];

        int i = 0;
        for (var value : ctx.nbtArrayValue()) {
            var literalVisitor = new LiteralVisitor(getScope());
            var literalValue = literalVisitor.visit(value);
            var literalType = literalValue.getType();

            switch (literalType) {
                case BYTE:
                    var bytee = (byte) literalValue.getValue();
                    if (type == LiteralType.INTEGER) {
                        ints[i] = bytee;
                    } else if (type == LiteralType.LONG) {
                        longs[i] = bytee;
                    } else {
                        bytes[i] = bytee;
                    }
                    break;
                case INTEGER:
                    if (type == LiteralType.BYTE) {
                        System.err.printf("Cannot add a literal of type %s to an array of type %s\n", literalType.name(), type.name());
                        return null;
                    }
                    var intt = (int) literalValue.getValue();
                    if (type == LiteralType.LONG) {
                        longs[i] = intt;
                    } else {
                        ints[i] = intt;
                    }
                    break;
                case LONG:
                    if (type != LiteralType.LONG) {
                        System.err.printf("Cannot add a literal of type %s to an array of type %s\n", literalType.name(), type.name());
                        return null;
                    }
                    longs[i] = (long) literalValue.getValue();
                    break;
            }

            i++;
        }

        switch (type) {
            case BYTE:
                return new ByteArrayTag(bytes);
            case INTEGER:
                return new IntArrayTag(ints);
            case LONG:
                return new LongArrayTag(longs);
        }

        return null;
    }

    @Override
    public ListTag visitNbtList(MOSParser.NbtListContext ctx) {
        if (ctx.nbtListValue().size() == 0) {
            System.err.println("Empty TAG_List! Unable to determine type.");
            return null;
        }

        var predictedType = predictTagType(ctx.nbtListValue(0).nbt());
        if (predictedType == null) {
            System.err.println("Unable to predict type for TAG_List.");
            return null;
        }

        var list = new ListTag(predictedType, new ArrayList<>());

        for (var valueContext : ctx.nbtListValue()) {
            list.add(visitChildren(valueContext));
        }

        return list;
    }

    @Override
    public Tag visitReference(MOSParser.ReferenceContext ctx) {
        var referenceVisitor = new ReferenceVisitor(getScope());
        var reference = referenceVisitor.visit(ctx);

        if (!reference.hasCapability(MOSTypeCapability.GetCompileTime)) {
            System.err.println("Reference has no 'GetCompileTime' capability.");
            return null;
        }

        var getResult = reference.getCompileTime();
        var get = getResult.getValue();

        if (get instanceof NBT) {
            return ((NBT) get).getValue();
        }

        if (get instanceof LiteralValue) {
            return NBTTools.createTagFromLiteralValue((LiteralValue) get);
        }

        System.err.printf("Cannot use reference to type %s as NBT tag.\n", get.getClass().getSimpleName());
        return null;
    }

    @Override
    public Tag visitLiteralValue(MOSParser.LiteralValueContext ctx) {
        var literalVisitor = new LiteralVisitor(getScope());
        var literalValue = literalVisitor.visit(ctx);

        return NBTTools.createTagFromLiteralValue(literalValue);
    }

    public static TagType predictTagType(MOSParser.NbtContext nbtContext) {
        if (nbtContext.nbtCompound() != null) {
            return TagType.COMPOUND;
        } else if (nbtContext.nbtArray() != null) {
            switch (nbtContext.nbtArray().nbtArrayType().getText()) {
                case "B":
                    return TagType.BYTE_ARRAY;
                case "I":
                    return TagType.INT_ARRAY;
                case "L":
                    return TagType.LONG_ARRAY;
            }
        } else if (nbtContext.nbtList() != null) {
            return TagType.LIST;
        } else if (nbtContext.reference() != null) {
            // TODO: Not supported a.t.m.

            return null;
        } else if (nbtContext.literalValue() != null) {
            var predictedLiteralType = LiteralVisitor.predictLiteralType(nbtContext.literalValue());
            if (predictedLiteralType != null) {
                switch (predictedLiteralType) {
                    case STRING:
                        return TagType.STRING;
                    case BOOLEAN:
                    case BYTE:
                        return TagType.BYTE;
                    case DOUBLE:
                        return TagType.DOUBLE;
                    case FLOAT:
                        return TagType.FLOAT;
                    case LONG:
                        return TagType.LONG;
                    case INTEGER:
                        return TagType.INT;
                    case SHORT:
                        return TagType.SHORT;
                }
            }
        }

        return null;
    }
}
