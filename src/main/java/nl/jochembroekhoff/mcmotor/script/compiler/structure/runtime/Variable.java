/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.IDProvider;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.range.RangeType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.EqualityOperator;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.AssignVariableToVariable;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.InitializeVariable;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalScore;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.ExecuteConditionalScoreMatches;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteChainPart;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.LiteralValueTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import nl.jochembroekhoff.mcmotor.script.type.defaults.BoolRT;

@Getter
@EqualsAndHashCode(callSuper = true)
public class Variable extends MOSType implements IProvidesMinecraftForm {

    String objective = Main.BUILD_CONFIG.getNamespace();
    String id = IDProvider.next(this);

    @Setter
    String sourceName;

    public Variable() {
        setStaticCapabilities(
                MOSTypeCapability.GetRunTime,
                MOSTypeCapability.Assign,
                MOSTypeCapability.Compare,
                MOSTypeCapability.ProvideExecuteChainPart,
                MOSTypeCapability.Operation,
                MOSTypeCapability.AssignWithOperation
        );
    }

    public Variable(Scope initialScope, int initialValue) {
        this();
        initialScope.getInstructions().add(new InitializeVariable(this, initialValue));
    }

    public Variable(Scope initialScope, int initialValue, String customIdPrefix) {
        this();
        id = IDProvider.next(customIdPrefix);
        initialScope.getInstructions().add(new InitializeVariable(this, initialValue));
    }

    public Variable(String id) {
        this();
        this.id = id;
    }

    public Variable(String objective, String id) {
        this();
        this.objective = objective;
        this.id = id;
    }

    /**
     * Clone a variable.
     * Note: this discards the value of "sourceName" if that is present in the given "otherVariable".
     *
     * @param otherVariable The variable to clone.
     */
    public Variable(Variable otherVariable) {
        this();
        this.objective = otherVariable.objective;
        this.id = otherVariable.id;
    }

    @Override
    public MOSResult<Variable> getRunTime(Scope runtimeScope) {
        return new MOSResult<>(true, this);
    }

    @Override
    public boolean assign(Scope currentScope, MOSType value) {
        if (!value.hasCapability(MOSTypeCapability.GetRunTime)) {
            System.err.printf("Cannot assign the value of type %s to this variable (%s) because it lacks the 'GetRunTime' capability.\n", value.getClass().getSimpleName(), sourceName);
            return false;
        }

        var getRunTimeResult = value.getRunTime(currentScope);
        if (!getRunTimeResult.isSuccess()) return false;

        currentScope.getInstructions().add(new AssignVariableToVariable(this, getRunTimeResult.getValue()));
        return true;
    }

    @Override
    public MOSResult<BoolRT> compare(Scope runtimeScope, EqualityOperator equalityOperator, MOSType valueToCompare) {

        /* Convert valueToCompare to a more usable form */

        if (valueToCompare instanceof LiteralValue) {
            if (((LiteralValue) valueToCompare).getType().isNumeric()) {
                valueToCompare = valueToCompare.getRunTime(runtimeScope).getValue();
            }
        }

        if (valueToCompare instanceof NBT) {
            if (NBTTools.isNumeric(((NBT) valueToCompare).getValue().getType())) {
                valueToCompare = new Variable(runtimeScope, NBTTools.getIntegerValue(((NBT) valueToCompare).getValue()));
            }
        }

        /* Perform real comparison */

        if (valueToCompare instanceof Variable) {
            var negate = false;
            var operator = equalityOperator.getMinecraftForm();

            if (equalityOperator == EqualityOperator.NotEqualTo) {
                negate = true;
                operator = "=";
            }

            var executeChainPart = new ExecuteConditionalScore(negate, this, operator, (Variable) valueToCompare);
            return new MOSResult<>(true, new BoolRT(executeChainPart));
        } else if (valueToCompare instanceof Range) {
            var executeChainPart = new ExecuteConditionalScoreMatches(false, this, (Range) valueToCompare);
            return new MOSResult<>(true, new BoolRT(executeChainPart));
        } else {
            System.err.printf("[Variable/Compare] Cannot compare the type %s.\n", valueToCompare.getClass().getSimpleName());
        }

        return new MOSResult<>(false, null);
    }

    /**
     * The execute chain part provided will check of the value of the variable at runtime is greater to zero.
     */
    @Override
    public MOSResult<IExecuteChainPart> provideExecuteChainPart(Scope runtimeScope) {
        var zero = Range.newRange(Integer.class);
        zero.setType(RangeType.BELOW);
        zero.setBelow(0);
        return new MOSResult<>(true, new ExecuteConditionalScoreMatches(true, this, zero));
    }

    @Override
    public MOSResult<? extends MOSType> operation(Scope runtimeScope, String operator, MOSType rightValue) {
        return null;
    }

    @Override
    public boolean assignWithOperation(Scope runtimeScope, String operator, MOSType value) {
        var isPlusOrMinus = operator.equals("+") || operator.equals("-");

        if (isPlusOrMinus && value instanceof LiteralValue && ((LiteralValue) value).getType().isWhole()) {
            var wholeValue = LiteralValueTools.getIntegerValue((LiteralValue) value);
            var addOrRemove = operator.equals("-")
                    ? "remove"
                    : "add";

            runtimeScope.getInstructions().add((IMinecraftInstruction) () -> "scoreboard players " + addOrRemove + " " + getMinecraftForm() + " " + wholeValue.toString());

            return true;
        }

        if (!value.hasCapability(MOSTypeCapability.GetRunTime)) {
            System.err.printf("Cannot perform assign-with-operation for the value of type %s to this variable (%s) because it lacks the 'GetRunTime' capability.\n", value.getClass().getSimpleName(), sourceName);
            return false;
        }

        var getRunTimeResult = value.getRunTime(runtimeScope);
        if (!getRunTimeResult.isSuccess()) return false;

        var mcOperation = operator + "=";

        runtimeScope.getInstructions().add((IMinecraftInstruction) () -> "scoreboard players operation " + getMinecraftForm() + " " + mcOperation + " " + getRunTimeResult.getValue().getMinecraftForm());

        return true;
    }

    @Override
    public String getMinecraftForm() {
        return id + " " + objective;
    }
}
