/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.StringTools;

public class LiteralVisitor extends MOSBaseVisitorWithScope<LiteralValue> {

    public LiteralVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public LiteralValue visitLiteralString(MOSParser.LiteralStringContext ctx) {
        var literalValue = new LiteralValue(LiteralType.STRING);
        literalValue.setValue(StringTools.removeQuotationMarksAndUnescape(ctx.String().getText()));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralBoolean(MOSParser.LiteralBooleanContext ctx) {
        var literalValue = new LiteralValue(LiteralType.BOOLEAN);
        literalValue.setValue(ctx.Boolean().getText().equals("true"));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralDouble(MOSParser.LiteralDoubleContext ctx) {
        var literalValue = new LiteralValue(LiteralType.DOUBLE);
        literalValue.setValue(Double.parseDouble(ctx.Double().getText()));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralFloat(MOSParser.LiteralFloatContext ctx) {
        var literalValue = new LiteralValue(LiteralType.FLOAT);
        literalValue.setValue(Float.parseFloat(ctx.Float().getText()));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralLong(MOSParser.LiteralLongContext ctx) {
        var literalValue = new LiteralValue(LiteralType.LONG);
        var text = ctx.Long().getText();
        if (text.endsWith("L") || text.endsWith("l"))
            text = text.substring(0, text.length() - 1);
        literalValue.setValue(Long.parseLong(text));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralInteger(MOSParser.LiteralIntegerContext ctx) {
        var literalValue = new LiteralValue(LiteralType.INTEGER);
        literalValue.setValue(Integer.parseInt(ctx.Integer().getText()));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralShort(MOSParser.LiteralShortContext ctx) {
        var literalValue = new LiteralValue(LiteralType.SHORT);
        var text = ctx.Short().getText();
        if (text.endsWith("S") || text.endsWith("s"))
            text = text.substring(0, text.length() - 1);
        literalValue.setValue(Short.parseShort(text));
        return literalValue;
    }

    @Override
    public LiteralValue visitLiteralByte(MOSParser.LiteralByteContext ctx) {
        var literalValue = new LiteralValue(LiteralType.BYTE);
        var text = ctx.Byte().getText();
        if (text.endsWith("B") || text.endsWith("b"))
            text = text.substring(0, text.length() - 1);
        literalValue.setValue(Byte.parseByte(text));
        return literalValue;
    }

    public static LiteralType predictLiteralType(MOSParser.LiteralValueContext literalValueContext) {
        if (literalValueContext.literalString() != null) {
            return LiteralType.STRING;
        } else if (literalValueContext.literalBoolean() != null) {
            return LiteralType.BOOLEAN;
        } else if (literalValueContext.literalNumeric() != null) {
            var numericContext = literalValueContext.literalNumeric();
            if (numericContext.literalDecimal() != null) {
                var decimalContext = numericContext.literalDecimal();
                if (decimalContext.literalDouble() != null) {
                    return LiteralType.DOUBLE;
                } else if (decimalContext.literalFloat() != null) {
                    return LiteralType.FLOAT;
                }
            } else if (numericContext.literalWhole() != null) {
                var wholeContext = numericContext.literalWhole();
                if (wholeContext.literalLong() != null) {
                    return LiteralType.LONG;
                } else if (wholeContext.literalInteger() != null) {
                    return LiteralType.INTEGER;
                } else if (wholeContext.literalShort() != null) {
                    return LiteralType.SHORT;
                } else if (wholeContext.literalByte() != null) {
                    return LiteralType.BYTE;
                }
            }
        }

        return null;
    }
}
