/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general.properties;

import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import net.querz.nbt.ByteTag;
import net.querz.nbt.StringTag;
import net.querz.nbt.TagType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.report.BlocksReportItem;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.LiteralValueTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.NBTTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@RequiredArgsConstructor
public class BlockReportBasedProperties implements IProperties, IProvidesMinecraftForm {

    private final BlocksReportItem reportItem;
    private final Map<String, LiteralType> propertyTypes = new HashMap<>();
    private volatile boolean propertyTypesCalculated = false;

    private final Map<String, LiteralValue> values = new HashMap<>();

    private void calculatePropertyTypes() {
        reportItem.getProperties().forEach((propertyName, propertyValues) -> {
            var allBoolean = true;
            var allInteger = true;

            for (var propertyValue : propertyValues) {
                if (!propertyValue.equals("true") && !propertyValue.equals("false")) {
                    allBoolean = false;
                }

                if (!StringUtils.isNumeric(propertyValue)) {
                    allInteger = false;
                }
            }

            var type = LiteralType.STRING;

            if (allBoolean) {
                type = LiteralType.BOOLEAN;
            } else if (allInteger) {
                type = LiteralType.INTEGER;
            }

            propertyTypes.put(propertyName, type);
        });
    }

    @Override
    public boolean setOrAddProperty(String key, boolean negate, MOSType value) {
        if (!reportItem.getProperties().containsKey(key)) {
            System.err.printf("Cannot set the value of %s: key not supported. Supported keys: %s.\n", key, StringUtils.join(reportItem.getProperties().keySet(), ", "));
            return false;
        }

        if (negate) {
            System.err.println("Block properties can't have negated values.");
            return false;
        }

        synchronized (propertyTypes) {
            if (!propertyTypesCalculated) {
                propertyTypesCalculated = true;
                calculatePropertyTypes();
            }
        }

        var propertyType = propertyTypes.get(key);
        var possibleValues = reportItem.getProperties().get(key);

        if (!value.hasCapability(MOSTypeCapability.GetCompileTime)) {
            System.err.printf("The type %s lacks the 'GetCompileTime' capability.", value.getClass().getSimpleName());
            return false;
        }

        var valueCompileTimeResult = value.getCompileTime();
        if (!valueCompileTimeResult.isSuccess()) {
            System.err.printf("Failed getting the compile time value of %s.", value.getClass().getSimpleName());
            return false;
        }

        var compileTimeValue = valueCompileTimeResult.getValue();

        if (compileTimeValue instanceof LiteralValue) {
            var literalValue = (LiteralValue) compileTimeValue;

            if (literalValue.getType() == LiteralType.STRING && propertyType == LiteralType.STRING) {
                var stringValue = literalValue.getValue().toString();
                if (possibleValues.contains(stringValue)) {
                    values.put(key, literalValue);
                } else {
                    System.err.printf("The properties container does not support setting the value of %s (STRING) to %s: not a valid value.\n", key, stringValue);
                    return false;
                }
            } else if (literalValue.getType().isNumeric() && propertyType == LiteralType.INTEGER) {
                var intValue = LiteralValueTools.getIntegerValue(literalValue, true);
                var intValueString = intValue.toString();
                if (possibleValues.contains(intValueString)) {
                    var newLiteralValue = new LiteralValue(LiteralType.INTEGER);
                    newLiteralValue.setValue(intValue);
                    values.put(key, newLiteralValue);

                    if (!literalValue.getType().isWhole()) {
                        // Warn the user if the value might not be whole numeric value
                        System.out.printf("Warning: The value of %s has been set to %d which may not be the given value as the given value is not a whole numeric value.\n", key, intValue);
                    }
                } else {
                    System.err.printf("The properties container does not support setting the value of %s (INTEGER) to %s: not a valid value.\n", key, intValue);
                    return false;
                }
            } else if (literalValue.getType() == LiteralType.BOOLEAN && propertyType == LiteralType.BOOLEAN) {
                values.put(key, literalValue);
            } else {
                System.err.printf("The properties container does not support setting the value of %s (type %s) to a LiteralValue of type %s.\n", key, propertyType, literalValue.getType());
                return false;
            }
        } else if (compileTimeValue instanceof NBT) {
            var nbtTag = ((NBT) compileTimeValue).getValue();

            if (nbtTag.getType() == TagType.STRING) {
                var stringValue = ((StringTag) nbtTag).getValue();
                if (possibleValues.contains(stringValue)) {
                    var literalValue = new LiteralValue(LiteralType.STRING);
                    literalValue.setValue(stringValue);
                    values.put(key, literalValue);
                } else {
                    System.err.printf("The properties container does not support setting the value of %s (STRING) to %s: not a valid value.\n", key, stringValue);
                    return false;
                }
            } else if (NBTTools.isNumeric(nbtTag.getType()) && propertyType == LiteralType.INTEGER) {
                var intValue = NBTTools.getIntegerValue(nbtTag, true);
                var intValueString = intValue.toString();
                if (possibleValues.contains(intValueString)) {
                    var newLiteralValue = new LiteralValue(LiteralType.INTEGER);
                    newLiteralValue.setValue(intValue);
                    values.put(key, newLiteralValue);

                    if (nbtTag.getType() == TagType.DOUBLE || nbtTag.getType() == TagType.FLOAT) {
                        // Warn the user if the value might not be whole numeric value
                        System.out.printf("Warning: The value of %s has been set to %d which may not be the given value as the given value is not a whole numeric value.\n", key, intValue);
                    }
                } else {
                    System.err.printf("The properties container does not support setting the value of %s (INTEGER) to %s: not a valid value.\n", key, intValue);
                    return false;
                }
            } else if (nbtTag.getType() == TagType.BYTE && propertyType == LiteralType.BOOLEAN) {
                var byteValue = ((ByteTag) nbtTag).getValue();
                if (byteValue != 0 && byteValue != 1) {
                    System.err.printf("The properties container does not support setting the value of %s (BOOLEAN) to %d: not a valid byte value, must either be 0 or 1.\n", key, byteValue);
                    return false;
                }

                var literalValue = new LiteralValue(LiteralType.BOOLEAN);
                literalValue.setValue(byteValue == 1);
                values.put(key, literalValue);
            } else {
                System.err.printf("The properties container does not support setting the value of %s (type %s) to a NBT tag of type %s.\n", key, propertyType, nbtTag.getType());
                return false;
            }
        } else {
            System.err.printf("The properties container does not support setting the value of %s (type %s) to value type %s.\n", key, propertyType, compileTimeValue.getClass().getSimpleName());
        }

        return true;
    }

    @Override
    public String getMinecraftForm() {
        val sb = new StringBuilder();

        values.forEach((key, value) -> {
            String literalValueFormatted;

            if (value.getType().isDecimal()) {
                literalValueFormatted = String.format(Locale.ROOT, "%f", value.getValue());
            } else {
                literalValueFormatted = value.getValue().toString();
            }

            sb.append(key)
                    .append('=')
                    .append(literalValueFormatted)
                    .append(',');
        });

        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }
}
