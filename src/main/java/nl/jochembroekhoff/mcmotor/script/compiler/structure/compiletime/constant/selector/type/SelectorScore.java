/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SelectorScore extends MOSType {

    private final Selector target;
    private static final String[] invocationSignature = {"objective"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<? extends MOSType> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        var transformed = transformArgumentsFromSignature(invocationSignature, argumentsUnnamed, argumentsNamed);

        var objectiveArgument = transformed.get("objective");
        var objective = CompileTimeTools.extractString(objectiveArgument);

        if (objective == null) {
            return new MOSResult<>(false, null);
        }

        if (objective.isEmpty()) {
            System.err.println("The objective value must not be empty.");
            return new MOSResult<>(false, null);
        }

        return new MOSResult<>(true, new Variable(objective, target.getMinecraftForm()));
    }
}
