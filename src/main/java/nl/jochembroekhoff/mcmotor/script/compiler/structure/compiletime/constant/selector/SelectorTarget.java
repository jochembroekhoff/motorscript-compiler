/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector;

import lombok.Getter;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;

public enum SelectorTarget implements IProvidesMinecraftForm {
    /**
     * Nearest player.
     */
    NEAREST_PLAYER("p", false),
    /**
     * Random player.
     */
    RANDOM_PLAYER("r", true),
    /**
     * All players. Including dead players.
     */
    ALL_PLAYERS("a", false),
    /**
     * All entities. Including dead players.
     */
    ALL_ENTITIES("e", false),
    /**
     * Executing entity. Including dead players.
     */
    EXECUTING_ENTITY("s", true);

    @Getter
    String minecraftForm;

    @Getter
    boolean isSingular;

    SelectorTarget(String minecraftForm, boolean isSingular) {
        this.minecraftForm = minecraftForm;
        this.isSingular = isSingular;
    }
}
