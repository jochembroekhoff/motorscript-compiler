/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.val;
import lombok.var;
import net.querz.nbt.CompoundTag;
import net.querz.nbt.TagType;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.IProperties;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.properties.BlockReportBasedProperties;
import nl.jochembroekhoff.mcmotor.script.registries.BlockRegistry;
import nl.jochembroekhoff.mcmotor.script.registries.GlobalRegistry;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class ResourceVisitor extends MOSBaseVisitorWithScope<Resource> {
    ResourceVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Resource visitResource(MOSParser.ResourceContext ctx) {
        var namespace = "minecraft";

        if (!ctx.Identifier().isEmpty()) {
            namespace = StringUtils.join(ctx.Identifier().stream().map(ParseTree::getText).toArray(), '.');
        } else if (ctx.Colon() != null) {
            namespace = Main.BUILD_CONFIG.getNamespace();
        }

        val pathParts = new ArrayList<String>();

        for (val identifier : ctx.resourceName().Identifier()) {
            pathParts.add(identifier.getText());
        }

        val path = StringUtils.join(pathParts, '/');

        /* Resource properties */

        var propertiesContainer = IProperties.newDefaultPropertiesContainer();

        // Try to use the BlockReportBasedProperties if this resource is a registered block
        val blockRegistryIndex = GlobalRegistry.getRegistryItems("minecraft", "block");
        if (GlobalRegistry.isRegistered(blockRegistryIndex, namespace, path)) {
            propertiesContainer = new BlockReportBasedProperties(BlockRegistry.get(GlobalRegistry.getFromIndex(blockRegistryIndex, namespace, path)));
        }

        if (ctx.properties() != null) {
            val propertiesVisitor = new PropertiesVisitor(getScope(), propertiesContainer);
            propertiesVisitor.visit(ctx.properties());
        }

        /* Resource NBT payload */

        var tag = new CompoundTag("");

        if (ctx.nbtCompound() != null) {
            val nbtVisitor = new NBTVisitor(getScope());
            val visitedTag = nbtVisitor.visit(ctx.nbtCompound());
            if (visitedTag.getType() != TagType.COMPOUND) {
                System.err.printf("Resource can only have a TAG_Compound as payload, got %s but expected COMPOUND.\n", visitedTag.getType());
            } else {
                tag = (CompoundTag) visitedTag;
            }
        }

        return new Resource(namespace, path, propertiesContainer, tag);
    }
}
