/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.tools;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;

public class LiteralValueTools {
    public static Integer getIntegerValue(LiteralValue literalValue, boolean allowAllNumeric) {
        if (literalValue == null)
            return null;

        var literalValueType = literalValue.getType();

        if (allowAllNumeric) {
            if (!literalValueType.isNumeric()) {
                System.err.printf("Unsupported literal value type %s to convert to Integer: all numeric values are allowed.\n", literalValueType.name());
                return null;
            }
        } else if (literalValueType != LiteralType.LONG && literalValueType != LiteralType.INTEGER && literalValueType != LiteralType.SHORT && literalValueType != LiteralType.BYTE) {
            System.err.printf("Unsupported literal value type %s to convert to Integer: only whole numeric values are allowed.\n", literalValueType.name());
            return null;
        }

        if (literalValueType == LiteralType.LONG) {
            var longValue = (long) literalValue.getValue();
            if (longValue > Integer.MAX_VALUE) {
                System.err.printf("Cannot implicitly cast literal value of type LONG to INTEGER because it's value (%d) exceeds Integer.MAX_VALUE (%d).\n", longValue, Integer.MAX_VALUE);
                return null;
            } else if (longValue < Integer.MIN_VALUE) {
                System.err.printf("Cannot implicitly cast literal value of type LONG to INTEGER because it's value (%d) is below Integer.MIN_VALUE (%d).\n", longValue, Integer.MIN_VALUE);
                return null;
            }
        }

        if (allowAllNumeric && (literalValueType == LiteralType.DOUBLE || literalValueType == LiteralType.FLOAT)) {
            // Check range if the value is DOUBLE or FLOAT
            var doubleValue = ((Number) literalValue.getValue()).doubleValue();
            if (doubleValue > Integer.MAX_VALUE) {
                System.err.printf("Cannot implicitly cast literal value of type %s to INTEGER because it's value (%f) exceeds Integer.MAX_VALUE (%d).\n", literalValueType, doubleValue, Integer.MAX_VALUE);
                return null;
            } else if (doubleValue < Integer.MIN_VALUE) {
                System.err.printf("Cannot implicitly cast literal value of type %s to INTEGER because it's value (%f) is below Integer.MIN_VALUE (%d).\n", literalValueType, doubleValue, Integer.MIN_VALUE);
                return null;
            }
        }

        return ((Number) literalValue.getValue()).intValue();
    }

    public static Integer getIntegerValue(LiteralValue literalValue) {
        return getIntegerValue(literalValue, false);
    }
}
