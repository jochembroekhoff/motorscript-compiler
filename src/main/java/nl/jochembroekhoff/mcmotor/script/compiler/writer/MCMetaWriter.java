/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.writer;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.PackMCMeta;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MCMetaWriter {
    public static void writeMCMeta() {
        var mcMetaFile = new File(Main.OUTPUT_DIR, "pack.mcmeta");

        PackMCMeta packMCMeta = null;

        if (mcMetaFile.exists()) {
            try (var reader = new FileReader(mcMetaFile)) {
                packMCMeta = Main.GSON_PRETTY.fromJson(reader, PackMCMeta.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (packMCMeta == null)
            packMCMeta = new PackMCMeta();

        packMCMeta.getPack().setPackFormat(4);

        if (Main.BUILD_CONFIG.getMcmeta().getDescription() != null) {
            packMCMeta.getPack().setDescription(Main.BUILD_CONFIG.getMcmeta().getDescription());
        }

        writeMCMeta(mcMetaFile, packMCMeta);
    }

    private static boolean writeMCMeta(File mcmetaFile, PackMCMeta data) {
        try (var writer = new FileWriter(mcmetaFile)) {
            Main.GSON_PRETTY.toJson(data, writer);
        } catch (IOException e) {
            return false;
        }

        return true;
    }
}
