/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.report;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.Collections;
import java.util.Map;

@Getter
public class BlocksReportState {
    private Map<String, String> properties = Collections.emptyMap();
    private int id = -1;
    @SerializedName("default")
    private boolean defauld = false;
}
