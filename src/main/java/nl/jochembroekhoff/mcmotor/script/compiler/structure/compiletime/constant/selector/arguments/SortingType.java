/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.selector.arguments;

import lombok.Getter;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IProvidesMinecraftForm;

public enum SortingType implements IProvidesMinecraftForm {
    NEAREST("nearest"),
    FURTHES("furthest"),
    RANDOM("random"),
    ARBITRATY("arbitrary");

    @Getter
    String minecraftForm;

    SortingType(String minecraftForm) {
        this.minecraftForm = minecraftForm;
    }

    public static SortingType reverseValue(String minecraftForm) {
        for (var item : SortingType.values()) {
            if (item.getMinecraftForm().equals(minecraftForm))
                return item;
        }
        throw new IllegalArgumentException("Could not find a valid SortingType for the given Minecraft form.");
    }
}
