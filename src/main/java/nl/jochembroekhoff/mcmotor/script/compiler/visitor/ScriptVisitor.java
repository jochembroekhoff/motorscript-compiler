/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.ArrayList;

public class ScriptVisitor extends MOSBaseVisitorWithScope<Scope> {

    public ScriptVisitor() {
        // Root/global scope
        super(new Scope(null));
    }

    @Override
    public Scope visitCompilerInstruction(MOSParser.CompilerInstructionContext ctx) {
        var compilerInstructionIdentifier = ctx.Identifier().getText();
        var compilerInstructionExpression = new ExpressionVisitor(getScope()).visit(ctx.expression());

        if (!compilerInstructionExpression.hasCapability(MOSTypeCapability.GetCompileTime)) {
            System.err.printf("The value of a compiler instruction must have the capability 'GetCompileTime'. The provided value of type %s lacks this capability.\n", compilerInstructionExpression.getClass().getSimpleName());
            return getScope();
        }

        var getCompileTime = compilerInstructionExpression.getCompileTime();

        if (!getCompileTime.isSuccess()) {
            System.err.printf("Failed performing 'GetCompileTime' on %s. Returned false.\n", compilerInstructionExpression.getClass().getSimpleName());
            return getScope();
        }

        if (!getScope().getCompilerInstructions().containsKey(compilerInstructionIdentifier)) {
            getScope().getCompilerInstructions().put(compilerInstructionIdentifier, new ArrayList<>());
        }

        getScope().getCompilerInstructions().get(compilerInstructionIdentifier).add(getCompileTime.getValue());

        return getScope();
    }

    @Override
    public Scope visitStatement(MOSParser.StatementContext ctx) {
        var statementVisitor = new StatementVisitor(getScope());
        statementVisitor.visit(ctx);

        return getScope();
    }

    @Override
    public Scope visitFunctionDeclaration(MOSParser.FunctionDeclarationContext ctx) {
        throw new UnsupportedOperationException("Custom functions are not supported at the moment.");
    }
}
