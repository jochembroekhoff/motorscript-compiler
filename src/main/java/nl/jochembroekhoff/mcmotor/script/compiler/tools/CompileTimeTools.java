/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.tools;

import lombok.val;
import net.querz.nbt.StringTag;
import net.querz.nbt.TagType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;

public class CompileTimeTools {

    public static String extractString(MOSType type, boolean warn) {
        val compileTimeType = TypeTools.performGetCompileTime(type, warn);

        if (compileTimeType == null) return null;

        if (compileTimeType instanceof LiteralValue) {
            val literalValue = (LiteralValue) compileTimeType;
            if (literalValue.getType() != LiteralType.STRING) {
                if (warn) {
                    System.err.printf("The LiteralValue provided is not of type STRING (but %s).\n", literalValue.getType());
                }
                return null;
            } else {
                return literalValue.getValue().toString();
            }
        } else if (compileTimeType instanceof NBT) {
            val tag = ((NBT) compileTimeType).getValue();
            if (tag.getType() != TagType.STRING) {
                if (warn) {
                    System.err.printf("The NBT tag provided is not of type STRING (but %s).\n", tag.getType());
                }
                return null;
            } else {
                return ((StringTag) tag).getValue();
            }
        } else {
            if (warn) {
                System.err.printf("Cannot extract string value from type %s.\n", compileTimeType.getClass().getSimpleName());
            }
            return null;
        }
    }

    public static String extractString(MOSType type) {
        return extractString(type, true);
    }

    public static Number extractWhole(MOSType type, long min, long max, boolean warn) {
        val compileTimeType = TypeTools.performGetCompileTime(type, warn);

        if (compileTimeType == null) return null;

        long extractedValue = 0;

        if (compileTimeType instanceof LiteralValue) {
            val literalValue = (LiteralValue) compileTimeType;

            if (!literalValue.getType().isWhole()) {
                if (warn) {
                    System.err.printf("The LiteralValue provided is not a whole numeric type (instead it is %s).\n", literalValue.getType());
                }
                return null;
            }

            extractedValue = ((Number) literalValue.getValue()).longValue();
        } else if (compileTimeType instanceof NBT) {
            val tag = ((NBT) compileTimeType).getValue();
            if (!NBTTools.isNumericWhole(tag.getType())) {
                if (warn) {
                    System.err.printf("The NBT tag provided is not a whole numeric type (instead it is %s).\n", tag.getType());
                }
                return null;
            }

            extractedValue = ((Number) tag.getValue()).longValue();
        } else {
            if (warn) {
                System.err.printf("Cannot extract a numeric whole value from type %s.\n", compileTimeType.getClass().getSimpleName());
            }
            return null;
        }

        if (extractedValue < min || extractedValue > max) {
            if (warn) {
                System.err.printf("Value is out of bounds. Value: %d, min: %d, max: %d.\n", extractedValue, min, max);
            }
            return null;
        }

        return extractedValue;
    }

    public static Number extractWhole(MOSType type, long min, long max) {
        return extractWhole(type, min, max, true);
    }

}
