/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.structure.general;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteChainPart;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.LiteralValueTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class LiteralValue extends MOSType {

    final LiteralType type;

    Object value;

    {
        setStaticCapabilities(
                MOSTypeCapability.GetCompileTime,
                MOSTypeCapability.AssignWithOperation
        );
    }

    @Override
    public boolean checkDynamicCapability(MOSTypeCapability capability) {
        switch (capability) {
            case GetRunTime:
                return getType().isNumeric();
            case ProvideExecuteChainPart:
                // ExecuteChainParts can only be provided when the value is of type BOOLEAN.
                return getType() == LiteralType.BOOLEAN;
            case Operation:
                // Cannot perform any operation on a LiteralValue of type BOOLEAN.
                return getType() != LiteralType.BOOLEAN;
            case ToConstantString:
                // TODO: Allow ToConstantString on all types. All values can be converted to a String...
                return getType() == LiteralType.STRING;
            case Negate:
                return getType() == LiteralType.BOOLEAN;
            default:
                return false;
        }
    }

    @Override
    public MOSResult<LiteralValue> getCompileTime() {
        return new MOSResult<>(true, this);
    }

    @Override
    public MOSResult<Variable> getRunTime(Scope runtimeScope) {
        var variable = new Variable(runtimeScope, LiteralValueTools.getIntegerValue(this));
        return new MOSResult<>(true, variable);
    }

    @Override
    public MOSResult<IExecuteChainPart> provideExecuteChainPart(Scope runtimeScope) {
        if (getType() != LiteralType.BOOLEAN) {
            return new MOSResult<>(false, null);
        }

        var boolValue = (Boolean) getValue();

        if (boolValue)
            return new MOSResult<>(true, () -> "");
        else
            return new MOSResult<>(true, () -> "unless entity @e[limit=1]");
    }

    @Override
    public MOSResult<? extends MOSType> operation(Scope runtimeScope, String operator, MOSType rightValue) {
        if (getType() == LiteralType.STRING) {
            if (operator.equals("+")) {
                if (!rightValue.hasCapability(MOSTypeCapability.GetCompileTime)) {
                    System.err.printf("The type %s lacks the 'GetCompileTime' capability. String concatenation cannot be performed.\n", rightValue.getClass().getSimpleName());
                    return new MOSResult<>(false, null);
                }

                var rightValueGetCT = rightValue.getCompileTime();
                if (!rightValueGetCT.isSuccess()) {
                    System.err.printf("The type %s failed to provide the value for 'GetCompileTime'.\n", rightValue.getClass().getSimpleName());
                    return new MOSResult<>(false, null);
                }
                var rightValueGetCTValue = rightValueGetCT.getValue();

                if (!rightValueGetCTValue.hasCapability(MOSTypeCapability.ToConstantString)) {
                    System.err.printf("The type %s lacks the 'ToConstantString' capability. String concatenation cannot be performed.\n", rightValue.getClass().getSimpleName());
                    return new MOSResult<>(false, null);
                }

                var toConstantStringResult = rightValueGetCTValue.toConstantString();
                // TODO: Check result
                var newString = getValue() + toConstantStringResult.getValue();
                var newLiteralValue = new LiteralValue(LiteralType.STRING);
                newLiteralValue.setValue(newString);
                return new MOSResult<>(true, newLiteralValue);
            } else {
                System.err.printf("Cannot perform the operation %s on a LiteralValue of type STRING.\n", operator);
                return new MOSResult<>(false, null);
            }
        } else if (getType().isNumeric()) {
            throw new UnsupportedOperationException("TODO: Implement LiteralValue <operation> MOSType for numeric values.");
        }

        return new MOSResult<>(false, null);
    }

    @Override
    public MOSResult<String> toConstantString() {
        return new MOSResult<>(true, getValue().toString());
    }

    @Override
    public MOSResult<? extends MOSType> negate() {
        if (getType() != LiteralType.BOOLEAN) return new MOSResult<>(false, null);
        var negatedLiteralValue = new LiteralValue(LiteralType.BOOLEAN);
        negatedLiteralValue.setValue(!((Boolean) getValue()));
        return new MOSResult<>(true, negatedLiteralValue);
    }
}
