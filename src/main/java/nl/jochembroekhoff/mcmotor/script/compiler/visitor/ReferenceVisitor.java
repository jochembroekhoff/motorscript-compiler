/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.val;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.IndexSegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.KeySegment;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.SegmentType;
import nl.jochembroekhoff.mcmotor.script.type.InternalFunctionsManager;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReferenceVisitor extends MOSBaseVisitorWithScope<MOSType> {
    ReferenceVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public MOSType visitReference(MOSParser.ReferenceContext ctx) {
        MOSType typeContainer = null;

        if (ctx.referenceInternal() != null) {
            val identifierCtx = ctx.referenceInternal().Identifier();
            val scheduleCtx = ctx.referenceInternal().Schedule();

            var internalReferenceName = "";

            if (identifierCtx != null) {
                internalReferenceName = identifierCtx.getText();
            } else if (scheduleCtx != null) {
                internalReferenceName = scheduleCtx.getText();
            }

            typeContainer = InternalFunctionsManager.newInternalFunctionInstance(internalReferenceName);
            if (typeContainer == null) {
                System.err.printf("Unknown or unsupported internal function %s.\n", internalReferenceName);
                return null;
            }
        } else if (ctx.referenceVarOrConst() != null) {
            val varOrConstName = ctx.referenceVarOrConst().Identifier().getText();
            if (getScope().isVarPresentInHierarchy(varOrConstName)) {
                typeContainer = getScope().getVariableFromHierarchy(varOrConstName);
            } else if (getScope().isConstPresentInHierarchy(varOrConstName)) {
                val constant = getScope().getConstantFromHierarchy(varOrConstName);
                typeContainer = constant.getType();
            } else {
                System.err.printf("The variable or constant \"%s\" is not present in the current scope's hierarchy.\n", varOrConstName);
            }
        }

        if (typeContainer == null) return null;

        return processReferenceParts(typeContainer, ctx.referencePart(), getScope());
    }

    public static MOSType processReferenceParts(MOSType targetType, List<MOSParser.ReferencePartContext> referenceParts, Scope scope) {
        for (var referencePart : referenceParts) {
            if (referencePart.path() != null) {
                var pathVisitor = new PathVisitor(scope);
                var path = pathVisitor.visit(referencePart.path());
                for (var partSegment : path.getSegments()) {
                    if (partSegment.getType() == SegmentType.INDEX) {
                        if (!targetType.hasCapability(MOSTypeCapability.FindByIndex)) {
                            System.err.printf("Container of type %s does not have the capability \"FindByIndex\".\n", targetType.getClass().getSimpleName());
                            return null;
                        }
                        var indexSegment = (IndexSegment) partSegment;
                        var findResult = targetType.find(indexSegment.getIndex());
                        // TODO: Check result
                        targetType = findResult.getValue();
                    } else if (partSegment.getType() == SegmentType.KEY) {
                        if (!targetType.hasCapability(MOSTypeCapability.FindByString)) {
                            System.err.printf("Container of type %s does not have the capability \"FindByString\".\n", targetType.getClass().getSimpleName());
                            return null;
                        }
                        var keySegment = (KeySegment) partSegment;
                        var findResult = targetType.find(keySegment.getKey());
                        if (!findResult.isSuccess()) {
                            System.err.printf("Failed executing 'FindByString' on the type %s. The type probably couldn't find by the given string (%s).\n", targetType.getClass().getSimpleName(), keySegment.getKey());
                        }
                        targetType = findResult.getValue();
                    }
                }
            }

            if (referencePart.referenceInvocation() != null) {
                if (!targetType.hasCapability(MOSTypeCapability.Invocation)) {
                    System.err.printf("Container of type %s does not have the capability \"Invocation\".\n", targetType.getClass().getSimpleName());
                    return null;
                }

                var rawUnnamedArguments = referencePart.referenceInvocation().arguments().argumentUnnamed();
                var rawNamedArguments = referencePart.referenceInvocation().arguments().argumentNamed();

                var unnamedArgumentsContainer = new ArrayList<MOSType>(rawUnnamedArguments.size());
                for (var rawNamedArgument : rawUnnamedArguments) {
                    var expressionVisitor = new ExpressionVisitor(scope);
                    unnamedArgumentsContainer.add(expressionVisitor.visit(rawNamedArgument));
                }

                var namedArgumentsContainer = new HashMap<String, MOSType>(rawNamedArguments.size());
                for (var rawNamedArgument : rawNamedArguments) {
                    var parameterName = rawNamedArgument.Identifier().getText();

                    if (namedArgumentsContainer.containsKey(parameterName)) {
                        System.err.printf("Cannot set the value of a named argument more then once (parameter %s).\n", parameterName);
                        return null;
                    }

                    var expressionVisitor = new ExpressionVisitor(scope);
                    namedArgumentsContainer.put(parameterName, expressionVisitor.visit(rawNamedArgument.expression()));
                }

                var invocationResult = targetType.invoke(scope, unnamedArgumentsContainer, namedArgumentsContainer);
                // TODO: Check result
                targetType = invocationResult.getValue();
            }
        }

        return targetType;
    }
}
