/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.compiler.visitor;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.antlr.MOSParser;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Path;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.path.Segment;

import java.util.LinkedList;

public class PathVisitor extends MOSBaseVisitorWithScope<Path> {
    PathVisitor(Scope initialScope) {
        super(initialScope);
    }

    @Override
    public Path visitPath(MOSParser.PathContext ctx) {
        var segments = new LinkedList<Segment>();

        for (var segmentContext : ctx.pathSegment()) {
            var segmentVisitor = new PathSegmentVisitor(getScope());
            segments.add(segmentVisitor.visit(segmentContext));
        }

        return new Path(segments);
    }
}
