/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.var;
import net.querz.nbt.CompoundTag;
import net.querz.nbt.Tag;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.NBT;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Position;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Summon extends MOSType {

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        if (argumentsUnnamed.size() < 2) {
            System.err.println("Cannot invoke /summon without less than two arguments.");
            return new MOSResult<>(false, null);
        }

        if (!(argumentsUnnamed.get(0) instanceof Resource)) {
            System.err.println("First argument of /summon must be a resource.");
            return new MOSResult<>(false, null);
        }

        if (!(argumentsUnnamed.get(1) instanceof Position)) {
            System.err.println("Second argument of /summon must be a position.");
            return new MOSResult<>(false, null);
        }

        var resource = (Resource) argumentsUnnamed.get(0);
        var position = (Position) argumentsUnnamed.get(1);
        final Tag tag;

        if (argumentsUnnamed.size() >= 3) {
            if (!(argumentsUnnamed.get(2) instanceof NBT)) {
                System.err.println("Third argument of \\summon must be NBT.");
                return new MOSResult<>(false, null);
            }
            tag = ((NBT) argumentsUnnamed.get(2)).getValue();
        } else {
            tag = new CompoundTag(Collections.emptyMap());
        }

        scope.getInstructions().add((IMinecraftInstruction) () -> {
            var sb = new StringBuilder();
            sb.append("summon ");
            sb.append(resource.getMinecraftForm());
            sb.append(' ');
            sb.append(position.getMinecraftForm());
            sb.append(' ');
            sb.append(tag.toTagString());
            return sb.toString();
        });

        return new MOSResult<>(true, new Void());
    }
}
