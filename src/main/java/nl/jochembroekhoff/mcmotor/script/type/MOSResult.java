/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class MOSResult<T> {
    /**
     * If the result is successful.
     */
    final boolean success;

    /**
     * Get the resulting value of the given type {@link T}.
     * Might be {@code null} if {@code getSuccess()} returns {@code false}.
     */
    final T value;

    static <TT> MOSResult<TT> newDefaultErrorInstance() {
        return new MOSResult<>(false, null);
    }
}
