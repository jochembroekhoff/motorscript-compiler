/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.type.defaults.*;
import nl.jochembroekhoff.mcmotor.script.type.defaults.schedule.Schedule;
import nl.jochembroekhoff.mcmotor.script.type.defaults.scoreboard.Scoreboard;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public final class InternalFunctionsManager {

    private static final Map<String, Class<? extends MOSType>> internalFunctions = new HashMap<>();

    public static void registerInternalFunction(Class<? extends MOSType> type, String name, String... alternatives) {
        internalFunctions.put(name, type);

        for (var alternative : alternatives) {
            internalFunctions.put(alternative, type);
        }
    }

    public static MOSType newInternalFunctionInstance(String name) {
        if (!internalFunctions.containsKey(name)) return null;

        try {
            return internalFunctions.get(name).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void registerDefaults() {
        registerInternalFunction(Random.class, "random");
        registerInternalFunction(Tellraw.class, "tellraw");
        registerInternalFunction(Summon.class, "summon");
        registerInternalFunction(Scoreboard.class, "scoreboard", "sb");
        registerInternalFunction(Schedule.class, "schedule");
        registerInternalFunction(Fill.class, "fill");
        registerInternalFunction(Setblock.class, "setblock");
    }
}
