/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.Main;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Selector;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.mcjson.chat.*;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Tellraw extends MOSType {

    public Tellraw() {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        if (argumentsUnnamed.size() < 2) {
            System.err.println("Tellraw requires at least two arguments.");
            return new MOSResult<>(false, null);
        }

        if (!(argumentsUnnamed.get(0) instanceof Selector)) {
            System.err.printf("First argument must be of type Selector. Got %s.\n", argumentsUnnamed.get(0).getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        var selector = (Selector) argumentsUnnamed.remove(0);
        var chatComponents = new ArrayList<ChatComponent>();

        for (var argument : argumentsUnnamed) {
            if (argument.hasCapability(MOSTypeCapability.GetCompileTime)) {
                var compileTimeResult = argument.getCompileTime();
                // TODO: Check result
                var compileTimeArgumentValue = compileTimeResult.getValue();
                if (compileTimeArgumentValue instanceof LiteralValue) {
                    chatComponents.add(new PlainTextComponent(((LiteralValue) compileTimeArgumentValue).getValue().toString()));
                } else if (compileTimeArgumentValue instanceof Selector) {
                    chatComponents.add(new SelectorComponent(((Selector) compileTimeArgumentValue).getMinecraftForm()));
                }
            } else if (argument.hasCapability(MOSTypeCapability.GetRunTime)) {
                var runTimeResult = argument.getRunTime(scope);
                // TODO: Check result
                var variable = runTimeResult.getValue();
                var scoreComponentInner = new ScoreComponentInner();
                scoreComponentInner.setName(variable.getId());
                scoreComponentInner.setObjective(variable.getObjective());
                chatComponents.add(new ScoreComponent(scoreComponentInner));
            }
        }

        scope.getInstructions().add((IMinecraftInstruction) () -> {
            var sb = new StringBuilder();
            sb.append("tellraw ");
            sb.append(selector.getMinecraftForm());
            sb.append(' ');
            sb.append(Main.GSON.toJson(chatComponents));
            return sb.toString();
        });

        return new MOSResult<>(true, new Void());
    }
}
