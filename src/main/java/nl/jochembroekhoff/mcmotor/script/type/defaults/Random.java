/*
 * Copyright 2018 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Range;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.LiteralValue;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.literal.LiteralType;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.List;
import java.util.Map;

public class Random extends MOSType {

    static final java.util.Random RANDOM = new java.util.Random();

    public Random() {
        setStaticCapabilities(
                MOSTypeCapability.Invocation
        );
    }

    @Override
    public MOSResult<LiteralValue> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        if (argumentsUnnamed.size() == 0) {
            var lv = new LiteralValue(LiteralType.INTEGER);
            lv.setValue(RANDOM.nextInt());
            return new MOSResult<>(true, lv);
        }

        if (argumentsUnnamed.size() == 1 && argumentsUnnamed.get(0) instanceof Range) {
            var range = (Range) argumentsUnnamed.get(0);
            LiteralValue lv = null;

            if (Double.class.isAssignableFrom(range.getClazz())) {
                lv = new LiteralValue(LiteralType.DOUBLE);
                // TODO: Support different types of ranges
            }

            if (Integer.class.isAssignableFrom(range.getClazz())) {
                lv = new LiteralValue(LiteralType.INTEGER);
            }

            if (lv != null) {
                return new MOSResult<>(true, lv);
            }
        }

        return new MOSResult<>(false, null);
    }
}
