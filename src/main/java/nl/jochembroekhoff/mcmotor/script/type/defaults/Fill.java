/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Position;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.TypeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.List;
import java.util.Map;

public class Fill extends MOSType {

    private static final String[] invocationSignatureRequired = {"from", "to", "block"};
    private static final String[] invocationSignatureOptional = {"old_block_handling", "replace_tile_name"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        val transformed = transformArgumentsFromSignature(invocationSignatureRequired, invocationSignatureOptional, argumentsUnnamed, argumentsNamed);

        val fromRaw = TypeTools.performGetCompileTime(transformed.get("from"));
        val toRaw = TypeTools.performGetCompileTime(transformed.get("to"));
        val blockRaw = TypeTools.performGetCompileTime(transformed.get("block"));

        // TODO: Implement different "old_block_handling". Implement "replace_tile_name", only if "old_block_handling" is "replace".

        if (!(fromRaw instanceof Position)) {
            System.err.printf("The provided argument for from is not a valid position, but %s.\n", fromRaw.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        if (!(toRaw instanceof Position)) {
            System.err.printf("The provided argument for from is not a valid position, but %s.\n", fromRaw.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        if (!(blockRaw instanceof Resource)) {
            System.err.printf("The provided argument for from is not a valid resource, but %s.\n", fromRaw.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        val from = (Position) fromRaw;
        val to = (Position) toRaw;
        val block = (Resource) blockRaw;

        scope.getInstructions().add((IMinecraftInstruction) () -> "fill " + from.getMinecraftForm(true) + " " + to.getMinecraftForm(true) + " " + block.getMinecraftForm());

        return new MOSResult<>(true, new Void());
    }
}
