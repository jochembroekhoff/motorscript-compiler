/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.ExecuteChain;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteChainPart;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteConditional;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BoolRT extends MOSType {

    {
        setStaticCapabilities(
                MOSTypeCapability.GetRunTime,
                MOSTypeCapability.ProvideExecuteChainPart,
                MOSTypeCapability.Negate
        );
    }

    final IExecuteConditional executeChainPart;

    /**
     * Provides the scoreboard name with the value <code>0</code> or <code>1</code>.
     *
     * @param runtimeScope
     * @return
     */
    @Override
    public MOSResult<Variable> getRunTime(Scope runtimeScope) {
        var output = new Variable(runtimeScope, 0);

        var executeChain = new ExecuteChain();
        executeChain.addChainPart(executeChainPart);
        executeChain.setRunInstruction(() -> "scoreboard players set " + output.getMinecraftForm() + " 1");
        runtimeScope.getInstructions().add(executeChain);

        return new MOSResult<>(true, output);
    }

    @Override
    public MOSResult<IExecuteChainPart> provideExecuteChainPart(Scope runtimeScope) {
        return new MOSResult<>(true, executeChainPart);
    }

    @Override
    public MOSResult<? extends MOSType> negate() {
        return new MOSResult<>(true, new BoolRT(executeChainPart.duplicate(!executeChainPart.isNegate())));
    }
}
