/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults;

import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.compiletime.constant.Position;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.TypeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class Setblock extends MOSType {

    private static final String[] allowedModes = {"replace", "destroy", "keep"};

    private static final String[] invocationSignatureRequired = {"position", "block"};
    private static final String[] invocationSignatureOptional = {"mode"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        val transformed = transformArgumentsFromSignature(invocationSignatureRequired, invocationSignatureOptional, argumentsUnnamed, argumentsNamed);

        val positionRaw = TypeTools.performGetCompileTime(transformed.get("position"));
        val blockRaw = TypeTools.performGetCompileTime(transformed.get("block"));
        val modeRawTransformed = transformed.get("mode");

        val modeRawString = modeRawTransformed == null
                ? null
                : CompileTimeTools.extractString(modeRawTransformed);

        if (!(positionRaw instanceof Position)) {
            System.err.printf("The provided argument for from is not a valid position, but %s.\n", positionRaw.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        if (!(blockRaw instanceof Resource)) {
            System.err.printf("The provided argument for block is not a valid resource, but %s.\n", blockRaw.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        val mode = modeRawString == null
                ? "replace"
                : modeRawString;

        if (!ArrayUtils.contains(allowedModes, mode)) {
            System.err.printf("The provided argument for mode is not a valid. It must be any of the following: %s.\n", StringUtils.join(allowedModes, ", "));
            return new MOSResult<>(false, null);
        }

        val position = (Position) positionRaw;
        val block = (Resource) blockRaw;

        scope.getInstructions().add((IMinecraftInstruction) () -> "setblock " + position.getMinecraftForm(true) + " " + block.getMinecraftForm() + " " + mode);

        return new MOSResult<>(true, new Void());
    }
}
