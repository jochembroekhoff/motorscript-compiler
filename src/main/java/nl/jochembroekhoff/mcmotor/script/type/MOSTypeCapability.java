/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type;

public enum MOSTypeCapability {
    /**
     * The type must implement the method <code>find(String segment)</code> which returns a {@link MOSResult} containing
     * the value of the given segment/property.
     */
    FindByString,
    /**
     * The type must implement the method <code>find(int index)</code> which returns a {@link MOSResult} containing the
     * value of the given index.
     */
    FindByIndex,
    /**
     * The type must implement the method <code>getRunTime()</code> which returns a {@link MOSResult} containing a
     * {@link nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable} which is a reference to the value
     * provided by this capability.
     */
    GetRunTime,
    /**
     * The type must implement the method <code>getCompileTime()</code> which returns a {@link MOSResult} containing
     * another type which can be passed around at compile time.
     */
    GetCompileTime,
    /**
     * The type must implement the method
     * <code>invoke(Scope scope, List&lt;{@link MOSType}&gt; argumentsNamed, Map&lt;String, {@link MOSType}&gt; argumentsNamed)</code>
     * which returns a {@link MOSResult} containing another type which can be passed around at compile time.
     * <p>
     * Invocations are not bound specifically to runtime or compile time. Only when the result of the invocation will be
     * used to assign to a new variable or constant, it will be checked.
     */
    Invocation,
    /**
     * The type must implement the method <code>assign(Scope runtimeScope, MOSType newValue)</code> which returns a
     * boolean indicating whether the assignment was successful.
     */
    Assign,
    /**
     * The type must implement the method <code>compare(Scope scope, EqualityOperator eqOp, MOSType rightExpr)</code>
     * which returns a {@link MOSResult} container which should return a string that is usable for an execute chain.
     */
    Compare,
    /**
     * The type must implement the method <code>provideExecuteChainPart(Scope runtimeScope)</code> which returns a
     * {@link MOSResult} container containing an implementation of
     * {@link nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteConditional} used to
     * perform boolean evaluation.
     */
    ProvideExecuteChainPart,
    /**
     * The type must implement the method <code>operation(Scope scope, String operator, MOSType rightExpression</code>
     * which returns a {@link MOSResult} container which should return the resulting type which can be used again.
     */
    Operation,
    /**
     * The type must implement the method <code>assignWithOperation(Scope runtimeScope, MOSType newValue)</code> which
     * returns a boolean indicating whether the assignment was successful.
     */
    AssignWithOperation,
    /**
     * The type must implement the method <code>toConstantString()</code> which returns a {@link MOSResult} containing
     * the created string.
     */
    ToConstantString,
    /**
     * The type must implement the method <code>negate()</code> which returns a {@link MOSResult} container which should
     * return the resulting type which can be used again.
     */
    Negate
}
