/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type;

import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.EqualityOperator;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.instruction.executechain.IExecuteChainPart;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.type.defaults.BoolRT;

import java.util.List;
import java.util.Map;

public interface IMOSType {
    default boolean checkDynamicCapability(MOSTypeCapability capability) {
        return false;
    }

    default MOSResult<? extends MOSType> find(String segment) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends MOSType> find(int index) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends MOSType> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<Variable> getRunTime(Scope runtimeScope) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends MOSType> getCompileTime() {
        return MOSResult.newDefaultErrorInstance();
    }

    default boolean assign(Scope currentScope, MOSType value) {
        return false;
    }

    default MOSResult<BoolRT> compare(Scope runtimeScope, EqualityOperator equalityOperator, MOSType valueToCompare) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends IExecuteChainPart> provideExecuteChainPart(Scope runtimeScope) {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends MOSType> operation(Scope runtimeScope, String operator, MOSType rightValue) {
        return MOSResult.newDefaultErrorInstance();
    }

    default boolean assignWithOperation(Scope runtimeScope, String operator, MOSType value) {
        return false;
    }

    default MOSResult<String> toConstantString() {
        return MOSResult.newDefaultErrorInstance();
    }

    default MOSResult<? extends MOSType> negate() {
        return MOSResult.newDefaultErrorInstance();
    }
}
