/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults.scoreboard;

import lombok.var;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.runtime.Variable;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;

import java.util.List;
import java.util.Map;

public class ScoreboardScore extends MOSType {

    private static final String[] invocationSignature = {"name", "objective"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Variable> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        var transformed = transformArgumentsFromSignature(invocationSignature, argumentsUnnamed, argumentsNamed);

        var name = CompileTimeTools.extractString(transformed.get("name"));
        var objective = CompileTimeTools.extractString(transformed.get("objective"));

        if (name == null || objective == null) {
            System.err.println("Invalid argument provided for the name and/or objective parameter(s).");
            return new MOSResult<>(false, null);
        }

        if (name.isEmpty()) {
            System.err.println("The name value must not be empty.");
            return new MOSResult<>(false, null);
        }

        if (objective.isEmpty()) {
            System.err.println("The objective value must not be empty.");
            return new MOSResult<>(false, null);
        }

        return new MOSResult<>(true, new Variable(objective, name));
    }
}
