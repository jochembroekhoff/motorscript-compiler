/*
 * Copyright 2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type.defaults.schedule;

import lombok.val;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.IMinecraftInstruction;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.Scope;
import nl.jochembroekhoff.mcmotor.script.compiler.structure.general.Resource;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.CompileTimeTools;
import nl.jochembroekhoff.mcmotor.script.compiler.tools.TypeTools;
import nl.jochembroekhoff.mcmotor.script.type.MOSResult;
import nl.jochembroekhoff.mcmotor.script.type.MOSType;
import nl.jochembroekhoff.mcmotor.script.type.MOSTypeCapability;
import nl.jochembroekhoff.mcmotor.script.type.defaults.Void;

import java.util.List;
import java.util.Map;

public class ScheduleFunction extends MOSType {

    private static final String[] invocationSignature = {"function", "time"};

    {
        setStaticCapabilities(MOSTypeCapability.Invocation);
    }

    @Override
    public MOSResult<Void> invoke(Scope scope, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        val transformed = transformArgumentsFromSignature(invocationSignature, argumentsUnnamed, argumentsNamed);

        val functionRaw = transformed.get("function");
        val timeRaw = CompileTimeTools.extractWhole(transformed.get("time"), 1, Long.MAX_VALUE);

        if (functionRaw == null || timeRaw == null) {
            System.err.println("Invalid argument provided for the function and/or time parameter(s).");
            return new MOSResult<>(false, null);
        }

        val functionCT = TypeTools.performGetCompileTime(functionRaw);

        if (!(functionCT instanceof Resource)) {
            System.err.printf("The provided argument for function is not a valid resource, but %s.\n", functionCT.getClass().getSimpleName());
            return new MOSResult<>(false, null);
        }

        val functionResource = (Resource) functionCT;

        // Finally add the corresponding instruction to the scope
        scope.getInstructions().add((IMinecraftInstruction) () -> "schedule function " + functionResource.getMinecraftForm(true) + " " + timeRaw.longValue());

        return new MOSResult<>(true, new Void());
    }
}
