/*
 * Copyright 2018-2019 Jochem Broekhoff
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.jochembroekhoff.mcmotor.script.type;

import lombok.val;
import lombok.var;

import java.util.*;
import java.util.stream.Collectors;

public abstract class MOSType implements IMOSType {

    private final BitSet staticCapabilities = new BitSet(MOSTypeCapability.values().length);

    /**
     * Set the given capabilities as static capabilities for this {@link MOSType}.
     * Existing set capabilities are kept.
     *
     * @param capabilitiesToSet Params of {@link MOSTypeCapability} to set.
     */
    protected final void setStaticCapabilities(MOSTypeCapability... capabilitiesToSet) {
        for (var capability : capabilitiesToSet) {
            staticCapabilities.set(capability.ordinal());
        }
    }

    /**
     * Remove the given capabilities as static capabilities from this {@link MOSType}.
     * Existing set capabilities are kept.
     *
     * @param capabilitiesToRemove Params of {@link MOSTypeCapability} to remove.
     */
    protected final void removeStaticCapabilities(MOSTypeCapability... capabilitiesToRemove) {
        for (var capability : capabilitiesToRemove) {
            staticCapabilities.clear(capability.ordinal());
        }
    }

    /**
     * Check if the {@link MOSType} has the given {@link MOSTypeCapability}. If the capability is not present in the
     * static capabilities, the capability is checked dynamically.
     *
     * @param capability The {@link MOSTypeCapability} to check.
     * @return If The given capability is enabled at this point in time.
     */
    public final boolean hasCapability(MOSTypeCapability capability) {
        if (!staticCapabilities.get(capability.ordinal())) {
            return checkDynamicCapability(capability);
        }

        return true;
    }

    protected static Map<String, MOSType> transformArgumentsFromSignature(String[] requiredParameters, String[] optionalParamters, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        val totalGivenArguments = argumentsUnnamed.size() + argumentsNamed.size();

        if (totalGivenArguments != requiredParameters.length) {
            System.err.printf("Amount of given parameters does not meet the requirement (%d given, %d required at least).", totalGivenArguments, requiredParameters.length);
        }

        val result = new HashMap<String, MOSType>(totalGivenArguments);

        var currentUnnamedIndex = 0;
        var unnamedSize = argumentsUnnamed.size();

        for (val requiredParameter : requiredParameters) {
            if (currentUnnamedIndex < unnamedSize) {
                result.put(requiredParameter, argumentsUnnamed.get(currentUnnamedIndex));
                argumentsUnnamed.set(currentUnnamedIndex, null);
                currentUnnamedIndex++;
            } else if (argumentsNamed.containsKey(requiredParameter)) {
                result.put(requiredParameter, argumentsNamed.remove(requiredParameter));
            } else {
                System.err.printf("Cannot find a matching argument for the required parameter %s.\n", requiredParameter);
            }
        }

        val filteredUnnamed = argumentsUnnamed.stream().filter(Objects::nonNull).collect(Collectors.toList());
        var filteredCurrentUnnamedIndex = 0;
        val filteredUnnamedSize = filteredUnnamed.size();

        for (val optionalParameter : optionalParamters) {
            if (filteredCurrentUnnamedIndex < filteredUnnamedSize) {
                result.put(optionalParameter, filteredUnnamed.get(filteredCurrentUnnamedIndex));
                filteredCurrentUnnamedIndex++;
            } else if (argumentsNamed.containsKey(optionalParameter)) {
                result.put(optionalParameter, argumentsNamed.remove(optionalParameter));
            }
        }

        return result;
    }

    protected static Map<String, MOSType> transformArgumentsFromSignature(String[] requiredParameters, List<MOSType> argumentsUnnamed, Map<String, MOSType> argumentsNamed) {
        return transformArgumentsFromSignature(requiredParameters, new String[0], argumentsUnnamed, argumentsNamed);
    }

}
