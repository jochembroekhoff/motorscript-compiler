# MotorScript Compiler

MotorScript is a programming language which compiles to Minecraft functions.
Currently, the compiler is experimental and only intended as a proof-of-concept.

Please head over to the official documentation page [here](https://jochembroekhoff.gitlab.io/motorscript-compiler/) to
check out the detailed documentation.