# The build configuration file

The build configuration file is a file which must be [TOML 0.4.0][1] compliant.
Below you can find details of available configuration properties.

All customizations can be done using the build configuration file.

# Default configuration

```toml
debug_mode = true
namespace = "motorscript"
inputs = []
output_folder = "output"

[mcmeta]
create = false
description = "MotorScript Compiler output datapack"
```

# Configuration details

## `debug_mode`

Type: boolean.

Indicates if the output is targeted for debug purposes or not.

## `namespace`

Type: string.

The namespace to be used in the generated datapack. Internally used by Minecraft to identify, separate and order
everything.

## `inputs`

Type: list of paths.

List of input files to be compiled. The paths are relative to the location of the build file.
The filenames in this list should not end with the `.mos` extension. This extension is appended automatically by the
compiler.

## `output_folder`

Type: path.

Path specifying the folder in which the compiler outputs its datapack contents.
This path is relative to the location of the build file.

## `mcmeta`

Type: object.

Contains details about the datapack metadata for Minecraft.

### `mcmeta.create`

Type: boolean.

Whether to create a new `pack.mcmeta` file in the output directory or not.

### `mcmeta.description`

Type: string.

Description to be put in the `pack.mcmeta` file. May contain [formatting codes][2].

[1]: https://github.com/toml-lang/toml/blob/master/versions/en/toml-v0.4.0.md
[2]: https://minecraft.gamepedia.com/Formatting_codes