# Usage

- Linux (`x86_64`/`amd64` only)
    - _No additional requirements if you use the native executable._
- All other platforms
    - Java 8 or above 

# Compilation

- JDK 8 or above
- Maven 3
- Optional (for generating native images):
    - GraalVM
    - UPX