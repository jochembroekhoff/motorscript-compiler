Welcome to the documentation of the MotorScript Compiler. This documentation contains details about how the compiler
works and details about the grammar and syntax of the language.

> **Please note** that MotorScript is experimental and may be unstable. Read more about this in the
> [development note](#development-note).

# What is MotorScript?

MotorScript is a simple programming language that compiles to Minecraft [functions][MCWiki/Function].
It's meant to help you develop your Minecraft functions faster, in a more convenient way. It's especially useful for map
makers who are working on large-scale projects.

Notable features:

- **99% type safety** which means that you'll catch a lot of errors at compile-time.
- Just like any programming language: if statements, loops, multi-line statements, etc.
- Far, far more easy to read and understand than regular Minecraft functions.

Compare the following two snippets, they work exactly the same:

```
// MotorScript code
as @e
{
    if @s.score("my_objective") >= 5
    {
        \tellraw(@a, "Score of ", @s, " is 5 or more")
    }
    else
    {
        \tellraw(@a, "Score of ", @s, " is less than 5")
    }
}
```

```
# Minecraft functions
execute as @e if score @s my_objective matches 5.. run tellraw @a ["Score of",{"selector":"@s"}," is 5 or more"]
execute as @e unless score @s my_objective matches 5.. run tellraw @a ["Score of",{"selector":"@s"}," is less than 5"]
```

You can compare MotorScript to older, inactive projects like [Command Block Parser by ZipKrowd][CBP] or
[Redstone Programming Language by tossha][RPL].

# Aim

The game "Minecraft" can be customized by executing [in-game commands][MCWiki/Commands].
With the release of [Minecraft: Java Edition 1.12][MCWiki/1.12] the [function system][MCWiki/Function] got introduced.
This allows players to run a list of commands using plain text files instead of having to deal with [command blocks][MCWiki/Command_Block].
Still, when you're working on a big project, it can be really tedious and time consuming having to write every command,
step by step. However, the commands that you can enter in those `.mcfunction` files, are still the regular commands that
one would enter in the chat bar or a command block. It's just a way to speed up command execution.

With MotorScript, the whole perspective of vanilla Minecraft programming might change. MotorScript provides a real
programming language which targets the Minecraft functions. MotorScript actually compiles one or more input files to a
datapack containing a bunch of `.mcfunction` files.

The aim is to provide a more convenient way to organize the programming part of map making or Minecraft programming.
Programming in MotorScript won't limit you in any way.

# Limitations

Don't put your expectations too high on MotorScript. It's a programming language, but actually acts more like a
preprocessor that outputs function files. This means that at this point no advanced features like subroutines are
supported (only using the unsafe `call` statement, at your own risk).
The same goes for complex run-time data structures, or run-time string operations. _Theoretically_ it's all possible.
But that's simply not implemented at this point in time, and won't likely be any time soon. For now, you have to be
creative and come up with your own solutions. Just like you have to do when you're using plain function files.

# Development note 

The current version is just called `1.0-SNAPSHOT`, and that means that there is no release.
At this point in time, nothing is fixed or definitive.
Anything might change at any point in time, without notice.

As this has been a project as an assingment for school (which I have finished), I do not intend to make any major progress
any more.
The project will be availabe as-is, without any guarantee about further development.
At least for now, the development of this proof-of-concept version will likely not continue, as it is only just a
proof-of-concept version.
In the future, I might start doing this project all over, but likely in a different programming language than Java.

[CBP]: http://zipkrowd.com/tools.htm#cbp
[RPL]: http://tossha.com/rpl/
[MCWiki/Commands]: https://minecraft.gamepedia.com/Commands
[MCWiki/Function]: https://minecraft.gamepedia.com/Function
[MCWiki/Command_Block]: https://minecraft.gamepedia.com/Command_Block
[MCWiki/1.12]: https://minecraft.gamepedia.com/1.12