Currently, no release builds are provided as this project is in snapshot development.
You can, however, grab the latest "bundle" artifacts created by the pipelines
[over here](https://gitlab.com/jochembroekhoff/motorscript-compiler/-/jobs/artifacts/master/download?job=bundle).
Please visit the page about requirements in order to find out more about 

# Invoking the compiler

To invoke the compiler, just execute binary JAR. For example: `java -jar motorscript-compiler.jar [args]`. If you're
using Linux on a x86_64 machine, you may use the native executable, so you can just run `./motorscript-compiler [args].`

When the compiler is invoked without additional arguments, the default settings are used.

At this point in time, there is one argument which can be set provided. This is the argument for the custom build file
location. The given path is relative. Invoking the compiler might look as follows:
`java -jar motorscript-compiler.jar src/_build.toml`.

# Examples

To get started quickly with MotorScript, you might want to check out the [examples][Repo/examples] in the GitLab repo.

[Repo/examples]: https://gitlab.com/jochembroekhoff/motorscript-compiler/tree/master/examples