#!/bin/bash

mkdir tmp
cd tmp

wget $1 -O server.jar

java -cp server.jar net.minecraft.data.Main --all

cp generated/reports/registries.json ../src/main/resources/reports/
cp generated/reports/blocks.json ../src/main/resources/reports/

cd ..

rm -rf tmp
